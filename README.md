# MicroJava Compiler

## Project description
This project is a compiler for the **MicroJava** language. It is written in Java and it is developed as a part of the course *Compiler Construction 1* at the School of Electrical Engineering, University of Belgrade.

## MicroJava language
MicroJava is a simple object-oriented language, similar to Java. 
It is statically typed, with strong type checking. It supports classes, methods, functions, global/local variables, constants, arrays and polymorphism. 
It does not support garbage collection.
Specification of the language can be found [here](docs/mikrojava_2023_2024_jan.pdf).

## Project structure
The project is divided into 4 parts:
- **Lexer** - lexical analysis of the source code. It uses the [.flex file](src/main/jflex/mjlexer.flex) to generate the lexer.
- **Parser** - syntactic analysis of the source code. It uses the [.cup file](src/main/cup/mjparser.cup) to generate the parser.
- **Semantic analysis** - semantic analysis of the source code.
- **Code generation** - code generation for the source code.

## Dependencies
Libraries used in this project:
- [Java](https://www.java.com/en/) - programming language
- [Gradle](https://gradle.org/) - build automation tool
- [JFlex](https://jflex.de/) - lexical analyzer generator for Java
- [JUnit](https://junit.org/junit4/) - unit testing framework for Java
- [Cup](https://www.cs.princeton.edu/~appel/modern/java/CUP/) - parser generator for Java
- [Slf4j](http://www.slf4j.org/) - simple logging facade for Java

## Usage
The project is written in Java 8. It can be built using Gradle.
- To generate and build the **parser** from *.cup* specification, use the following command:
    ```bash
    ./gradlew generateParser
    ```

- To generate and build **lexer** from *.flex* file, use the following command:
    ```bash
    ./gradlew generateLexer
    ```

- To disassemble the generated *.obj* file run:
    ```bash
    ./gradlew disassemble [ > test/out/out.txt ]
    ```

- To run *.obj* file, use the following command:
    ```bash
    ./gradlew runObjFile
    ```

- To run the tests, use the following command:
    ```bash
    ./gradlew test
    ```
  
- To delete all generated files by *lexer* and *parser*, use the following command:
    ```bash
    ./gradlew deleteGenerated
    ```