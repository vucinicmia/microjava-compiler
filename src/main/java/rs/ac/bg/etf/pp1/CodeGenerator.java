package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.attributes.OperatorType;
import rs.ac.bg.etf.pp1.jump_states.ForLoopJumpsState;
import rs.ac.bg.etf.pp1.jump_states.IfCondition;
import rs.ac.bg.etf.pp1.jump_states.IfElseJumpsState;
import rs.ac.bg.etf.pp1.utils.NameMangler;
import rs.ac.bg.etf.pp1.visitor_states.CodeGeneratorState;
import rs.ac.bg.etf.pp1.visitors.IfElseStatementConditionsCollector;
import rs.ac.bg.etf.pp1.visitors.UnpackDesignatorCounter;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.util.*;

public class CodeGenerator extends VisitorAdaptor {
    private static final String MAIN_METHOD_NAME = "main";
    private static final String CHR_METHOD_NAME = "chr";
    private static final String ORD_METHOD_NAME = "ord";
    private static final String LEN_METHOD_NAME = "len";
    private static final String BOOL = "bool";
    private static final Struct boolType = Tab.find(BOOL).getType();
    private static final int INTEGER_WIDTH = 5;
    private static final int CHAR_WIDTH = 1;
    private final CodeGeneratorState state = new CodeGeneratorState();

    public int getMainPc() {
        return state.getMainPc();
    }

    public CodeGenerator() {
        generateCodeForPredefinedFunctions();
    }

    private void generateCodeForPredefinedFunctions() {
        state.addPredefinedMethod(ORD_METHOD_NAME, Code.pc);
        generateCodeForFunction(List.of(Code.load_n));

        state.addPredefinedMethod(CHR_METHOD_NAME, Code.pc);
        generateCodeForFunction(List.of(Code.load_n));

        state.addPredefinedMethod(LEN_METHOD_NAME, Code.pc);
        generateCodeForFunction(List.of(Code.load_n, Code.arraylength));
    }

    private void generateCodeForFunction(List<Integer> instructions) {
        Code.put(Code.enter);
        Code.put(1);
        Code.put(1);
        instructions.forEach(Code::put);
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

    // *** CLASS DECLARATION ***

    private void visitClassDeclStart(ClassDeclStart classDecl) {
        final Obj classObj = classDecl.obj;
        state.setCurrentClass(classObj);
        state.addDeclaredClass(classObj);
    }

    @Override
    public void visit(ClassDeclStartWithoutStatic classDecl) {
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticVarDecl classDecl) {
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticInitializer classDecl) {
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticVarDeclAndStaticInitializer classDecl) {
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDecl classDecl) {
        state.setCurrentClass(null);
    }

    @Override
    public void visit(SingleStaticInitializer staticInitializer) {
        state.addStaticInitializer(staticInitializer.getStaticInitializer());
    }

    @Override
    public void visit(StaticInitializerList staticInitializerList) {
        state.addStaticInitializer(staticInitializerList.getStaticInitializer());
    }

    private void generateVFT() {
        final int numberOfVariables = SemanticAnalyzer.getNumberOfVariables();
        final int[] variableCounter = {numberOfVariables};

        state.getDeclaredClasses().forEach(declaredClass -> {
            final Struct type = declaredClass.getType();
            final String declaredClassName = declaredClass.getName();

            state.addVftEntryForClass(declaredClassName);
            state.setVftTableForClass(declaredClassName, variableCounter[0]);

            type.getMembers()
                    .stream()
                    .filter(symbol -> symbol.getKind() == Obj.Meth)
                    .forEach(method -> {
                        final String methodName = NameMangler.demangle(method.getName()); // TODO - name_mangling
                        for (int i = 0; i < methodName.length(); i++) {
                            Code.loadConst(methodName.charAt(i));
                            Code.put(Code.putstatic);
                            Code.put2(variableCounter[0]);
                            variableCounter[0]++;
                        }
                        Code.loadConst(-1); // -1 -> used to mark method name end
                        Code.put(Code.putstatic);
                        Code.put2(variableCounter[0]);
                        variableCounter[0]++;
                        final int methodAddress = method.getAdr();

                        final Struct baseClassType = type.getElemType();

                        if (methodAddress == 0 && baseClassType != Tab.noType) {
                            final Optional<Obj> baseClassMethod = type.getElemType().getMembers()
                                    .stream()
                                    .filter(m -> NameMangler.demangle(m.getName()).equals(methodName))
                                    .findFirst();
                            baseClassMethod.ifPresent(m -> {
                                method.setAdr(m.getAdr());
                                Code.loadConst(m.getAdr());
                            });
                        } else {
                            Code.loadConst(methodAddress); // load method address
                        }
                        Code.put(Code.putstatic);
                        Code.put2(variableCounter[0]);
                        variableCounter[0]++;
                    });
            Code.loadConst(-2); // -2 -> used to mark end of TVF for class
            Code.put(Code.putstatic);
            Code.put2(variableCounter[0]);
            variableCounter[0]++;
        });
        SemanticAnalyzer.setNumberOfVariables(variableCounter[0]);
    }

    // *** METHOD DECLARATION ***

    private void visitMethodName(String methodName, MethodTypeName methodTypeName) {
        if (methodName.equals(MAIN_METHOD_NAME)) {
            state.setMainPc(Code.pc);
            generateVFT();
            state.getStaticInitializersList()
                    .forEach(
                            staticInitializer -> staticInitializer.traverseBottomUp(this)
                    );
        }
        final Obj methodObj = methodTypeName.obj;

        methodObj.setAdr(Code.pc);
        Code.put(Code.enter);

        final int numberOfFormalParameters = methodObj.getLevel();
        Code.put(numberOfFormalParameters);

        final int numberOfLocalVariables = methodObj.getLocalSymbols().size();
        Code.put(numberOfLocalVariables);
    }

    @Override
    public void visit(ReturnTypeMethodName methodName) {
        visitMethodName(methodName.getMethodName(), methodName);
    }

    @Override
    public void visit(VoidMethodName methodName) {
        visitMethodName(methodName.getMethodName(), methodName);
    }

    @Override
    public void visit(MethodDecl methodDecl) {
        final Obj methodObj = methodDecl.getMethodTypeName().obj;
        final Struct methodType = methodObj.getType();
        if (methodType == Tab.noType) {
            Code.put(Code.exit);
            Code.put(Code.return_);
        } else {
            Code.put(Code.trap);
            Code.put(1);
        }
    }

    // *** STATEMENT ***

    private HashMap<Integer, IfCondition> generateIfConditionsHashMap(LinkedList<LinkedList<CondFact>> condFactList) {
        /**
         * Terminology:
         *      if (condition):
         *          then:
         *
         *          end_then;
         *  [
         *      else:
         *          then:
         *
         *          end_then
         *  ]
         *      after_then;
         * Explanation:
         * When we are evaluating some boolean expression, we can add some optimizations.
         * - For example:
         *    a == 1 && b == 2 && c == 3
         *    If a == 1 is false, then we do not need to evaluate b == 2 and c == 3. We can jump to the else statement
         *    (if present) or after_then, because we know that the whole expression is false.
         * - Another example:
         *    a == 1 && b == 2 || c == 3 || d == 4 && e == 5
         *    - If a == 1 is false, then we do not need to evaluate b == 2. We can jump to the next || (or) statement
         *    c == 3.
         *    - If c == 3 is true, then we do not need to evaluate d == 4 && e == 5. We can jump to the then
         *    statement, because we know that the whole expression is true.
         *    - If neither a == 1 && b == 2 nor c == 3 is true, then we need to evaluate d == 4 && e == 5. If d == 4 is false,
         *    then we do not need to evaluate e == 5. We can jump to the else statement (if present) or after_then,
         *    because we know that the whole expression is false. If d == 4 is true, then we need to evaluate e == 5.
         *    If e == 5 is true, then we can jump to the then statement, because we know that the whole expression is
         *    true. If e == 5 is false, then we can jump to the else statement (if present) or after_then, because we
         *    know that the whole expression is false.
         */
        final int condFactListSize = condFactList.size();
        final HashMap<Integer, IfCondition> ifConditionsMap = new HashMap<>();
        final boolean oneRow = condFactListSize == 1;

        for (int orRow = 0; orRow < condFactListSize; orRow++) {
            final LinkedList<CondFact> condFactLinkedList = condFactList.get(orRow);
            for (int andCol = 0; andCol < condFactLinkedList.size(); andCol++) {
                final CondFact condFactFromList = condFactLinkedList.get(andCol);
                final boolean lastRow = orRow == condFactListSize - 1;
                final boolean lastInRow = andCol == condFactLinkedList.size() - 1;

                final IfCondition ifCondition = new IfCondition(condFactFromList);
                ifCondition.setInLastRow(lastRow);
                ifCondition.setLastInRow(lastInRow);
                ifCondition.setInOnlyRow(oneRow);
                ifConditionsMap.put(condFactFromList.hashCode(), ifCondition);
            }
        }

        return ifConditionsMap;
    }

    @Override
    public void visit(IfStart ifStart) {
        final SyntaxNode parent = ifStart.getParent();
        final IfElseStatementConditionsCollector ifElseStatementConditionsCollector = new IfElseStatementConditionsCollector();
        parent.traverseTopDown(ifElseStatementConditionsCollector);

        final LinkedList<LinkedList<CondFact>> condFactList = ifElseStatementConditionsCollector.getCondFactList();
        final HashMap<Integer, IfCondition> ifConditionsMap = generateIfConditionsHashMap(condFactList);

        final IfElseJumpsState ifElseJumpsState = new IfElseJumpsState(ifConditionsMap);
        if (ifStart.getParent() instanceof StatementIfElse) {
            ifElseJumpsState.setHasElse(true);
        }
        state.pushIfElseJumpState(ifElseJumpsState);
    }

    @Override
    public void visit(AfterIfCondition afterIfCondition) {
        state.peekIfElseJumpStates().ifPresent(jumpState -> jumpState.getOnThenJumpAddressList().forEach(Code::fixup));
    }

    @Override
    public void visit(Else elseStatement) {
        state.peekIfElseJumpStates().ifPresent(jumpState -> {
            if (jumpState.isHasElse()) {
                Code.putJump(0);
                final int fixupAddr = Code.pc - 2; // on after then jump
                jumpState.getOnElseJumpAddressList().forEach(Code::fixup);
                jumpState.addAfterThenJumpAddress(fixupAddr);
            }
        });
    }

    @Override
    public void visit(StatementIf statement) {
        final IfElseJumpsState ifElseJumpState = state.popIfElseJumpState();
        ifElseJumpState.getAfterThenJumpAddressList().forEach(Code::fixup);
    }

    @Override
    public void visit(StatementIfElse statement) {
        final IfElseJumpsState ifElseJumpState = state.popIfElseJumpState();
        ifElseJumpState.getAfterThenJumpAddressList().forEach(Code::fixup);
    }

    @Override
    public void visit(StatementBreak statement) {
        Code.putJump(0);
        state.peekForLoopJumpStates().ifPresent(
                jumpState -> jumpState.addAfterLoopJumpAddress(Code.pc - 2)
        );
    }

    @Override
    public void visit(StatementContinue statement) {
        Code.putJump(0);
        state.peekForLoopJumpStates().ifPresent(
                jumpState -> jumpState.addOnUpdationStatementJumpAddress(Code.pc - 2)
        );
    }

    @Override
    public void visit(StatementReturnExpr statement) {
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

    @Override
    public void visit(StatementReturn statement) {
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

    @Override
    public void visit(StatementRead statement) {
        final Obj designatorObj = statement.getDesignator().obj;
        if (designatorObj.getType() == Tab.intType) {
            Code.put(Code.read);
        } else if (designatorObj.getType() == Tab.charType) {
            Code.put(Code.bread);
        }
        Code.store(designatorObj);
    }

    @Override
    public void visit(StatementPrint statement) {
        final Struct type = statement.getExpr().struct;
        if (type == Tab.intType || type == boolType) {
            Code.loadConst(INTEGER_WIDTH);
            Code.put(Code.print);
        } else if (type == Tab.charType) {
            Code.loadConst(CHAR_WIDTH);
            Code.put(Code.bprint);
        }
    }

    @Override
    public void visit(StatementPrintWithWidth statementPrint) {
        final int width = statementPrint.getWidth();
        Code.loadConst(width);
        final Struct exprType = statementPrint.getExpr().struct;
        if (exprType == Tab.intType) {
            Code.put(Code.print);
        } else if (exprType == Tab.charType) {
            Code.put(Code.bprint);
        }
    }

    @Override
    public void visit(StatementFor statement) {
        final ForLoopJumpsState forLoopJumpsState = state.popForLoopJumpState();
        forLoopJumpsState.getAfterLoopJumpAddressList().forEach(Code::fixup);
    }

    @Override
    public void visit(ForLoopStart statement) {
        final ForLoopJumpsState forLoopJumpsState = new ForLoopJumpsState();
        state.pushForLoopJumpState(forLoopJumpsState);
    }

    @Override
    public void visit(ForLoopConditionStart statement) {
        state.peekForLoopJumpStates().ifPresent(
                jumpState -> jumpState.setConditionCheckAddress(Code.pc)
        );
    }

    @Override
    public void visit(ForLoopCondition condition) {
        state.peekForLoopJumpStates().ifPresent(
                jumpState -> {
                    jumpState.addAfterLoopJumpAddress(Code.pc - 2);
                    final StatementFor statementFor = (StatementFor) condition.getParent();
                    statementFor.getForLoopBody().traverseBottomUp(this);
                }
        );
    }

    @Override
    public void visit(UpdationStatementStart statement) {
        state.peekForLoopJumpStates().ifPresent(
                jumpState -> jumpState.getOnUpdationStatementJumpAddressList().forEach(Code::fixup)
        );
    }

    private void visitUpdationStatement(UpdationStatementListForLoop statement) {
        if (statement.getParent() instanceof StatementFor) {
            state.peekForLoopJumpStates().ifPresent(
                    jumpState -> Code.putJump(jumpState.getConditionCheckAddress())
            );
        }
    }

    @Override
    public void visit(ForLoopUpdationStatementList statement) {
        visitUpdationStatement(statement);
    }

    @Override
    public void visit(ForLoopSingleUpdationStatement statement) {
        visitUpdationStatement(statement);
    }

    @Override
    public void visit(NoForLoopUpdationStatement statement) {
        visitUpdationStatement(statement);
    }

    // *** DESIGNATOR STATEMENT ***

    @Override
    public void visit(DesignatorStatementAssignopExpr designatorStatement) {
        final Obj designatorObj = designatorStatement.getDesignator().obj;
        Code.store(designatorObj);
    }

    private void visitDesignatorIncrementOrDecrement(boolean isDesignatorArray, Obj designatorObj, int operation) {
        /*
         * Problem:
         * - In case of array, if we do not duplicate the array address and index,
         *   we will not know where to store the result.
         * - Wrong code:
         *      getstatic 1
         *      const_0                  element value
         *      aload                    const_1
         *      const_1          =>      add
         *      add                      store
         *      astore
         * - Correct code:
         *      getstatic 1              getstatic 1 # array address
         *      const_0                  const_0     # array index
         *      dup2                     element value
         *      aload            =>      add
         *      const_1                  astore
         *      add
         *      astore
         */
        if (isDesignatorArray) {
            Code.put(Code.dup2);
        }
        Code.load(designatorObj);
        Code.loadConst(1);
        Code.put(operation);
        Code.store(designatorObj);
    }

    @Override
    public void visit(DesignatorStatementMethodCallNoActPars designatorStatement) {
        final Obj methodObj = designatorStatement.getMethodCallNoActPars().obj;
        if (methodObj.getType() != Tab.noType) {
            Code.put(Code.pop);
        }
    }

    @Override
    public void visit(DesignatorStatementMethodCallWithActPars designatorStatement) {
        final Obj methodObj = designatorStatement.getMethodCallWithActPars().obj;
        if (methodObj.getType() != Tab.noType) {
            Code.put(Code.pop);
        }
    }

    /**
     * Returns class in which the method is defined.
     *
     * @param methodObj
     * @return
     */
    private Optional<Obj> isMethodClassMember(Obj methodObj) {
        final List<Obj> declaredClasses = state.getDeclaredClasses();
        for (Obj declaredClass : declaredClasses) {
            final Optional<Obj> foundMethod = declaredClass.getType().getMembers()
                    .stream()
                    .filter(methodObj::equals)
                    .findFirst();
            if (foundMethod.isPresent()) {
                return Optional.of(declaredClass);
            }
        }
        return Optional.empty();
    }

    private void visitFunctionCall(MethodCallStart methodCallStart, Obj methodObj) {
        final String methodName = NameMangler.demangle(methodObj.getName()); // TODO - name_mangling
        final boolean isMethodClassMember = isMethodClassMember(methodObj).isPresent();
        final boolean isMethodCalledOnObject = methodCallStart.getDesignator() instanceof DesignatorClassMember;
        if (isMethodClassMember) {
            if (isMethodCalledOnObject) {
                // Example: obj.method()
                methodCallStart.getDesignator().traverseBottomUp(this);
            }

            Code.put(Code.getfield); // vft address
            Code.put2(0);

            Code.put(Code.invokevirtual);

            methodName.chars().forEach(Code::put4);

            Code.put4(-1);

        } else {
            Code.put(Code.call);
            if (state.isPredefinedMethod(methodName)) {
                state.getPredefinedMethodStartAddress(methodName).ifPresent(address -> Code.put2(address - Code.pc + 1));
                return;
            }
            final int offset = methodObj.getAdr() - Code.pc + 1;
            Code.put2(offset);
        }
    }

    public void visit(MethodCallNoActPars methodCall) {
        final Obj methodObj = methodCall.obj;
        visitFunctionCall(methodCall.getMethodCallStart(), methodObj);
        state.setCurrentlyInvokedMethod(null);
    }

    public void visit(MethodCallWithActPars methodCall) {
        final Obj methodObj = methodCall.obj;
        visitFunctionCall(methodCall.getMethodCallStart(), methodObj);
        state.setCurrentlyInvokedMethod(null);
    }

    @Override
    public void visit(DesignatorStatementIncrement designatorStatement) {
        final boolean isDesignatorArray = designatorStatement.getDesignator() instanceof DesignatorArray;
        final Obj designatorObj = designatorStatement.getDesignator().obj;
        visitDesignatorIncrementOrDecrement(isDesignatorArray, designatorObj, Code.add);
    }

    @Override
    public void visit(DesignatorStatementDecrement designatorStatement) {
        final boolean isDesignatorArray = designatorStatement.getDesignator() instanceof DesignatorArray;
        final Obj designatorObj = designatorStatement.getDesignator().obj;
        visitDesignatorIncrementOrDecrement(isDesignatorArray, designatorObj, Code.sub);
    }

    @Override
    public void visit(DesignatorStatementUnpackStart designatorStatementUnpackStart) {
        final UnpackDesignatorCounter unpackDesignatorCounter = new UnpackDesignatorCounter();
        designatorStatementUnpackStart.getParent().traverseBottomUp(unpackDesignatorCounter);
        final int unpackVariablesCounter = unpackDesignatorCounter.getUnpackDesignatorsCounter();
        final Obj sourceArrayObj = unpackDesignatorCounter.getSourceArrayObj();

        // if length of sourceArrayObj < size then trap

        Code.load(sourceArrayObj);
        Code.put(Code.arraylength);
        Code.loadConst(unpackVariablesCounter);
        Code.putFalseJump(Code.le, 0);
        final int trapAddress = Code.pc - 2;
        Code.put(Code.trap);
        Code.put(1);

        Code.fixup(trapAddress);
    }

    @Override
    public void visit(DesignatorStatementUnpack designatorStatementUnpack) {
        final LinkedList<Designator> unpackDesignators = state.getUnpackDesignators();
        final int size = unpackDesignators.size();
        final Obj sourceArrayObj = designatorStatementUnpack.getDesignator1().obj;
        final Obj destinationArrayObj = designatorStatementUnpack.getDesignator().obj;

        for (int i = size - 1; i >= 0; i--) {
            final Designator unpackDesignator = unpackDesignators.get(i);
            if (unpackDesignator == null) {
                continue;
            }
            Code.load(sourceArrayObj);
            Code.loadConst(i);
            Code.put(Code.aload);
            Code.store(unpackDesignators.get(i).obj);
        }

        /*
         *      for (int i = size; i < sourceArrayObj.size(); i++){
         *          destinationArrayObj[i - size] = sourceArrayObj[i]
         *      }
         */

        // i = size
        Code.loadConst(size);
        Code.put(Code.putstatic);
        Code.put2(0);

        // Condition check -> i < sourceArrayObj.length
        final int conditionCheckAddress = Code.pc;
        Code.put(Code.getstatic);
        Code.put2(0);
        Code.load(sourceArrayObj);
        Code.put(Code.arraylength);
        Code.putFalseJump(Code.lt, 0);
        final int afterLoopJumpAddress = Code.pc - 2;

        // destinationArrayObj[i - size]
        Code.load(destinationArrayObj);
        Code.put(Code.getstatic);
        Code.put2(0);
        Code.loadConst(size);
        Code.put(Code.sub);

        // sourceArrayObj[i]
        Code.load(sourceArrayObj);
        Code.put(Code.getstatic);
        Code.put2(0);
        Code.put(Code.aload);

        Code.put(Code.astore); // destinationArrayObj[i - size] = sourceArrayObj[i]

        // i++
        Code.put(Code.getstatic);
        Code.put2(0);
        Code.loadConst(1);
        Code.put(Code.add);
        Code.put(Code.putstatic);
        Code.put2(0);

        Code.putJump(conditionCheckAddress);

        Code.fixup(afterLoopJumpAddress);

        state.clearUnpackDesignators();
    }

    @Override
    public void visit(DesignatorPresent designatorPresent) {
        state.unpackDesignatorsCounter++;
        state.addUnpackDesignator(designatorPresent.getDesignator());
    }

    @Override
    public void visit(NoDesignator noDesignator) {
        state.unpackDesignatorsCounter++;
        state.addUnpackDesignator(null);
    }

    // *** CONDITION TERM ***

    private void visitConditionTerm(CondFact condFact) {
        final int fixupAddr = Code.pc - 2;
        final Optional<IfElseJumpsState> ifElseJumpState = state.peekIfElseJumpStates();
        ifElseJumpState.ifPresent(
                jumpState -> {
                    final IfCondition ifCondition = jumpState.getIfCondition(condFact.hashCode());
                    if (ifCondition.isInOnlyRow()) {
                        if (jumpState.isHasElse()) {
                            jumpState.addOnElseJumpAddress(fixupAddr);
                        } else {
                            jumpState.addAfterThenJumpAddress(fixupAddr);
                        }
                    } else {
                        final boolean lastRow = ifCondition.isInLastRow();
                        final boolean lastInRow = ifCondition.isLastInRow();
                        if (lastInRow && !lastRow) {
                            jumpState.addOnThenJumpAddress(fixupAddr);
                        } else if (!lastInRow && !lastRow) {
                            jumpState.addOnNextOrJumpAddress(fixupAddr);
                        } else {
                            if (jumpState.isHasElse()) {
                                jumpState.addOnElseJumpAddress(fixupAddr);
                            } else {
                                jumpState.addAfterThenJumpAddress(fixupAddr);
                            }
                        }
                    }
                }
        );
    }

    @Override
    public void visit(SingleCondTerm singleCondTerm) {
        final CondFact condFact = singleCondTerm.getCondFact();
        visitConditionTerm(condFact);
    }

    @Override
    public void visit(CondTermAndCondFact condTermAndCondFact) {
        final CondFact condFact = condTermAndCondFact.getCondFact();
        visitConditionTerm(condFact);
    }

    @Override
    public void visit(OrOperator orOperator) {
        state.peekIfElseJumpStates().ifPresent(
                jumpState -> {
                    jumpState.getOnNextOrJumpAddressList().forEach(Code::fixup);
                    jumpState.getOnNextOrJumpAddressList().clear();
                }
        );
    }

    // *** CONDITION FACT ***

    @Override
    public void visit(CondFactExprRelopExpr condFact) {
        boolean falseJump = isFalseJump(condFact);
        final OperatorType operatorType = condFact.getRelop().operatortype;
        if (operatorType == OperatorType.EQUAL) {
            Code.putFalseJump(falseJump ? Code.eq : Code.inverse[Code.eq], 0);
        } else if (operatorType == OperatorType.NOT_EQUAL) {
            Code.putFalseJump(falseJump ? Code.ne : Code.inverse[Code.ne], 0);
        } else if (operatorType == OperatorType.GREATER_THAN) {
            Code.putFalseJump(falseJump ? Code.gt : Code.inverse[Code.gt], 0);
        } else if (operatorType == OperatorType.GREATER_OR_EQUAL) {
            Code.putFalseJump(falseJump ? Code.ge : Code.inverse[Code.ge], 0);
        } else if (operatorType == OperatorType.LESS_THAN) {
            Code.putFalseJump(falseJump ? Code.lt : Code.inverse[Code.lt], 0);
        } else if (operatorType == OperatorType.LESS_OR_EQUAL) {
            Code.putFalseJump(falseJump ? Code.le : Code.inverse[Code.le], 0);
        }
    }

    @Override
    public void visit(SingleCondFact condFact) {
        Code.put(Code.const_1);
        boolean falseJump = isFalseJump(condFact);
        Code.putFalseJump(falseJump ? Code.eq : Code.inverse[Code.eq], 0);
    }

    private boolean isFalseJump(CondFact condFact) {
        if (condFact.getParent() instanceof ForLoopCondition) {
            return true;
        }

        final Optional<IfElseJumpsState> ifElseJumpStateOptional = state.peekIfElseJumpStates();
        final IfElseJumpsState ifElseJumpState = ifElseJumpStateOptional.orElseThrow(
                () -> new RuntimeException("isFalseJump: IfElseJumpsState is not present")
        );

        final IfCondition ifCondition = ifElseJumpState.getIfCondition(condFact.hashCode());
        if (ifCondition.isInOnlyRow()) {
            return true;
        } else {
            final boolean lastRow = ifCondition.isInLastRow();
            final boolean lastInRow = ifCondition.isLastInRow();
            if (lastInRow && !lastRow) {
                return false;
            } else if (!lastInRow && !lastRow) {
                return true;
            } else {
                return true;
            }
        }
    }

    // *** EXPR ***

    @Override
    public void visit(ExprMinusTerm term) {
        Code.put(Code.neg);
    }

    @Override
    public void visit(ExprAddopTerm expr) {
        final OperatorType operatorType = expr.getAddop().operatortype;
        if (operatorType == OperatorType.PLUS) {
            Code.put(Code.add);
        } else if (operatorType == OperatorType.MINUS) {
            Code.put(Code.sub);
        }
    }

    // *** TERM ***

    @Override
    public void visit(TermMulopFactor term) {
        final OperatorType operatorType = term.getMulop().operatortype;
        if (operatorType == OperatorType.MULTIPLY) {
            Code.put(Code.mul);
        } else if (operatorType == OperatorType.DIVIDE) {
            Code.put(Code.div);
        } else if (operatorType == OperatorType.MOD) {
            Code.put(Code.rem);
        }
    }

    // *** FACTOR ***

    @Override
    public void visit(DesignatorFactor designatorFactor) {
        final Obj designatorObj = designatorFactor.getDesignator().obj;
        Code.load(designatorObj);
    }

    @Override
    public void visit(NumberConst numberConst) {
        final int value = numberConst.getValue();
        Code.loadConst(value);
    }

    @Override
    public void visit(CharConst charConst) {
        final char value = charConst.getValue();
        Code.loadConst(value);
    }

    @Override
    public void visit(BooleanConst boolConst) {
        final boolean value = boolConst.getValue();
        Code.loadConst(value ? 1 : 0);
    }

    @Override
    public void visit(NewArray newArray) {
        final Struct arrayType = newArray.getType().struct;
        Code.put(Code.newarray);
        if (arrayType == Tab.charType) {
            Code.put(0);
        } else {
            Code.put(1);
        }
    }

    private Optional<Obj> getClassObjFromType(Struct type) {
        final List<Obj> declaredClasses = state.getDeclaredClasses();
        for (Obj declaredClass : declaredClasses) {
            if (declaredClass.getType().equals(type)) {
                return Optional.of(declaredClass);
            }
        }
        return Optional.empty();
    }

    @Override
    public void visit(NewClassObject newClassObject) {
        final Struct classType = newClassObject.getType().struct;
        getClassObjFromType(classType).ifPresent(
                classObj -> {
                    Code.put(Code.new_);
                    Code.put2(classObj.getType().getNumberOfFields() * 4);
                    Code.put(Code.dup);

                    final String className = classObj.getName();
                    state.getVftEntryAddress(className).ifPresentOrElse(
                            Code::loadConst,
                            () -> {
                                Code.put(Code.const_);
                                Code.put4(0);
                                state.addToFixAddressForClass(className, Code.pc - 2);
                            }
                    );

                    Code.put(Code.putfield);
                    Code.put2(0);
                }
        );
    }

    // *** DESIGNATOR ***

    public void visitDesignatorIdent(Designator designator) {
        final Obj designatorObj = designator.obj;
        final int designatorKind = designatorObj.getKind();

        final boolean isDesignatorArray = designator.getParent() instanceof DesignatorArray;
        final boolean isClassMethod = isMethodClassMember(designatorObj).isPresent();

        if (designatorKind == Obj.Meth) {
            state.setCurrentlyInvokedMethod(designatorObj);
        }

        if (designatorObj.getType().getKind() == Struct.Class &&
                (designatorKind == Obj.Fld || designatorKind == Obj.Elem || designatorKind == Obj.Var)) {
            state.setCurrentObject(designatorObj);
        }

        if (isDesignatorArray) {
            Code.load(designatorObj);
        } else if (designatorKind == Obj.Fld || designatorKind == Obj.Meth && isClassMethod) {
            if (state.isInsideClass()) {
                Code.put(Code.load_n);
                if (state.isInInvokedMethod()) {
                    Code.put(Code.dup);
                }
            } else {
                final Obj currentlyInvokedMethod = state.getCurrentlyInvokedMethod();
                final Obj currentObject = state.getCurrentObject();
                final boolean isMethodCalledOnCurrentObject =
                        currentlyInvokedMethod != null &&
                                currentObject != null &&
                                currentObject.getType().getMembers()
                                        .stream()
                                        .anyMatch(currentlyInvokedMethod::equals);

                if (state.isInInvokedMethod() && isMethodCalledOnCurrentObject) {
                    Code.load(currentObject);
                }
            }
        }
    }

    @Override
    public void visit(DesignatorIdentWithNamespace designator) {
        visitDesignatorIdent(designator);
    }

    @Override
    public void visit(DesignatorIdentWithoutNamespace designator) {
        visitDesignatorIdent(designator);
    }

    @Override
    public void visit(DesignatorClassMember designator) {
        visitDesignatorIdent(designator);
    }

    @Override
    public void visit(DesignatorArray designator) {
        visitDesignatorIdent(designator);
    }

}
