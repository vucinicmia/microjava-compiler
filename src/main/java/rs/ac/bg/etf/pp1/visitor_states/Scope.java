package rs.ac.bg.etf.pp1.visitor_states;

public enum Scope {
    GLOBAL,
    CLASS_DECL,
    STATIC_CLASS_VAR_DECL,
    METHOD_DECL,
    FOR_LOOP
}
