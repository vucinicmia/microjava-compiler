package rs.ac.bg.etf.pp1.visitor_states;

import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class SemanticAnalyzerState {

    private Obj currentMethod = null;
    private Obj currentClass = null;
    private String currentNamespace = null;
    private Struct currentType = null;
    private int level = -1;

    private boolean insideStaticInitializer = false;
    private final Stack<Scope> scope = new Stack<>();

    private final List<Struct> currentMethodArgumentTypes = new LinkedList<>();
    private final List<Struct> currentUnpackDesignatorTypes = new LinkedList<>();

    public Obj getCurrentMethod() {
        return currentMethod;
    }

    public void setCurrentMethod(Obj currentMethod) {
        this.currentMethod = currentMethod;
    }

    public boolean isInsideMethod() {
        return currentMethod != null;
    }

    public boolean isInsideClass() {
        return currentClass != null;
    }

    public String getCurrentNamespace() {
        return currentNamespace;
    }

    public void setCurrentNamespace(String currentNamespace) {
        this.currentNamespace = currentNamespace;
    }

    public Struct getCurrentType() {
        return currentType;
    }

    public void setCurrentType(Struct currentType) {
        this.currentType = currentType;
    }

    public void setCurrentClass(Obj currentClass) {
        this.currentClass = currentClass;
    }

    public Obj getCurrentClass() {
        return currentClass;
    }

    public void openScope(Scope scope) {
        this.scope.push(scope);
        level++;
    }

    public void closeScope() {
        this.scope.pop();
        level--;
    }

    public Scope getCurrentScope() {
        return this.scope.peek();
    }

    public int getLevel() {
        return level;
    }

    public boolean isInsideStaticInitializer() {
        return insideStaticInitializer;
    }

    public void setInsideStaticInitializer(boolean insideStaticInitializer) {
        this.insideStaticInitializer = insideStaticInitializer;
    }

    public void addMethodArgumentType(Struct struct) {
        currentMethodArgumentTypes.add(struct);
    }

    public List<Struct> getCurrentMethodArgumentTypes() {
        return currentMethodArgumentTypes;
    }

    public void clearCurrentMethodArgumentTypes() {
        currentMethodArgumentTypes.clear();
    }

    public void addUnpackDesignatorType(Struct struct) {
        currentUnpackDesignatorTypes.add(struct);
    }

    public List<Struct> getCurrentUnpackDesignatorTypes() {
        return currentUnpackDesignatorTypes;
    }

    public void clearCurrentUnpackDesignatorTypes() {
        currentUnpackDesignatorTypes.clear();
    }

}
