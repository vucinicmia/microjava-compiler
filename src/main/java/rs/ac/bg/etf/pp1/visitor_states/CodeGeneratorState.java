package rs.ac.bg.etf.pp1.visitor_states;

import rs.ac.bg.etf.pp1.ast.Designator;
import rs.ac.bg.etf.pp1.ast.StaticInitializer;
import rs.ac.bg.etf.pp1.jump_states.ForLoopJumpsState;
import rs.ac.bg.etf.pp1.jump_states.IfElseJumpsState;
import rs.etf.pp1.symboltable.concepts.Obj;

import java.util.*;

public class CodeGeneratorState {
    private int mainPc;
    private final HashMap<String, Integer> predefinedMethodsStartAddressMap = new HashMap<>();
    private final Stack<IfElseJumpsState> ifElseJumpStatesStack = new Stack<>();
    private final Stack<ForLoopJumpsState> forLoopJumpStatesStack = new Stack<>();
    private final LinkedList<Designator> unpackDesignators = new LinkedList<>();
    private final List<Obj> declaredClasses = new LinkedList<>();
    private final Map<String, VFTEntry> classNameToVftEntryAddressMap = new HashMap<>();
    private final List<StaticInitializer> staticInitializersList = new LinkedList<>();
    private Obj currentClass = null;
    private Obj currentlyInvokedMethod = null;
    private Obj currentObject = null;

    public int unpackDesignatorsCounter = 0;

    public int getMainPc() {
        return mainPc;
    }

    public void setMainPc(int mainPc) {
        this.mainPc = mainPc;
    }

    public void addPredefinedMethod(final String methodName, final int methodStartAddress) {
        predefinedMethodsStartAddressMap.put(methodName, methodStartAddress);
    }

    public Optional<Integer> getPredefinedMethodStartAddress(final String methodName) {
        return Optional.ofNullable(predefinedMethodsStartAddressMap.get(methodName));
    }

    public boolean isPredefinedMethod(final String methodName) {
        return predefinedMethodsStartAddressMap.containsKey(methodName);
    }

    public void pushIfElseJumpState(final IfElseJumpsState ifElseJumpAddresses) {
        ifElseJumpStatesStack.push(ifElseJumpAddresses);
    }

    public IfElseJumpsState popIfElseJumpState() {
        return ifElseJumpStatesStack.pop();
    }

    public Optional<IfElseJumpsState> peekIfElseJumpStates() {
        return ifElseJumpStatesStack.empty() ? Optional.empty() : Optional.of(ifElseJumpStatesStack.peek());
    }

    public void pushForLoopJumpState(final ForLoopJumpsState forLoopJumpState) {
        forLoopJumpStatesStack.push(forLoopJumpState);
    }

    public ForLoopJumpsState popForLoopJumpState() {
        return forLoopJumpStatesStack.pop();
    }

    public Optional<ForLoopJumpsState> peekForLoopJumpStates() {
        return forLoopJumpStatesStack.empty() ? Optional.empty() : Optional.of(forLoopJumpStatesStack.peek());
    }

    public void addUnpackDesignator(final Designator designator) {
        unpackDesignators.add(designator);
    }

    public void clearUnpackDesignators() {
        unpackDesignators.clear();
    }

    public LinkedList<Designator> getUnpackDesignators() {
        return unpackDesignators;
    }

    public void addDeclaredClass(final Obj classObj) {
        declaredClasses.add(classObj);
    }

    public List<Obj> getDeclaredClasses() {
        return declaredClasses;
    }

    public void addVftEntryForClass(String className) {
        final VFTEntry vftEntry = new VFTEntry();
        classNameToVftEntryAddressMap.put(className, vftEntry);
    }

    public void setVftTableForClass(final String className, final int vftTableAddress) {
        final VFTEntry vftEntry = classNameToVftEntryAddressMap.get(className);
        if (vftEntry == null) {
            final VFTEntry newVftEntry = new VFTEntry();
            newVftEntry.setAddress(vftTableAddress);
            classNameToVftEntryAddressMap.put(className, newVftEntry);
            return;
        }
        vftEntry.setAddress(vftTableAddress);
    }

    public Optional<Integer> getVftEntryAddress(final String className) {
        final VFTEntry vftEntry = classNameToVftEntryAddressMap.get(className);
        if (vftEntry == null) {
            final VFTEntry newVftEntry = new VFTEntry();
            classNameToVftEntryAddressMap.put(className, newVftEntry);
            return Optional.empty();
        }
        return vftEntry.getAddress();
    }

    public void addToFixAddressForClass(final String className, final int toFixAddress) {
        final VFTEntry vftEntry = classNameToVftEntryAddressMap.get(className);
        if (vftEntry == null) {
            final VFTEntry newVftEntry = new VFTEntry();
            newVftEntry.addToFixAddress(toFixAddress);
            classNameToVftEntryAddressMap.put(className, newVftEntry);
            return;
        }
        vftEntry.addToFixAddress(toFixAddress);
    }

    public void setCurrentClass(final Obj currentClass) {
        this.currentClass = currentClass;
    }

    public boolean isInsideClass() {
        return currentClass != null;
    }

    public Obj getCurrentlyInvokedMethod() {
        return currentlyInvokedMethod;
    }

    public void setCurrentlyInvokedMethod(Obj currentlyInvokedMethod) {
        this.currentlyInvokedMethod = currentlyInvokedMethod;
    }

    public boolean isInInvokedMethod() {
        return currentlyInvokedMethod != null;
    }

    public Obj getCurrentObject() {
        return currentObject;
    }

    public void setCurrentObject(Obj currentObject) {
        this.currentObject = currentObject;
    }

    public void addStaticInitializer(StaticInitializer staticInitializer) {
        staticInitializersList.add(staticInitializer);
    }

    public List<StaticInitializer> getStaticInitializersList() {
        return staticInitializersList;
    }

}
