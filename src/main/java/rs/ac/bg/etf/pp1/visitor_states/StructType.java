package rs.ac.bg.etf.pp1.visitor_states;

import rs.etf.pp1.symboltable.concepts.Struct;

public class StructType {
    public static String getTypeName(int type) {
        return switch (type) {
            case Struct.None -> "None";
            case Struct.Int -> "Int";
            case Struct.Char -> "Char";
            case Struct.Array -> "Array";
            case Struct.Class -> "Class";
            case Struct.Bool -> "Bool";
            case Struct.Enum -> "Enum";
            case Struct.Interface -> "Interface";
            default -> "Unknown";
        };
    }
}
