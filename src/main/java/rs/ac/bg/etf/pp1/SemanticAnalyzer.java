package rs.ac.bg.etf.pp1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.attributes.OperatorType;
import rs.ac.bg.etf.pp1.utils.NameMangler;
import rs.ac.bg.etf.pp1.visitor_states.Scope;
import rs.ac.bg.etf.pp1.visitor_states.StructType;
import rs.ac.bg.etf.pp1.visitor_states.SemanticAnalyzerState;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class SemanticAnalyzer extends VisitorAdaptor {
    private static final String BOOL_TYPE = "bool";
    private static final String THIS_VAR = "this";
    private static final String V_TABLE_PTR = "vPointer";
    private static final String DESIGNATOR_COUNTER = "__designator_counter";

    private static final Logger log = LoggerFactory.getLogger(SemanticAnalyzer.class);

    private final SemanticAnalyzerState state = new SemanticAnalyzerState();
    private static int numberOfVariables;

    private boolean errorDetected = false;

    SemanticAnalyzer() {
        Tab.insert(Obj.Type, BOOL_TYPE, new Struct(Struct.Bool));
    }

    public boolean isErrorDetected() {
        return errorDetected;
    }

    public static int getNumberOfVariables() {
        return numberOfVariables;
    }

    public static void setNumberOfVariables(int numberOfVariables) {
        SemanticAnalyzer.numberOfVariables = numberOfVariables;
    }

    public void logError(String message, SyntaxNode info) {
        errorDetected = true;
        final int line = (info == null) ? 0 : info.getLine();
        log.error(message + " on line " + line);
    }

    public void logInfo(String message, SyntaxNode info) {
        final int line = (info == null) ? 0 : info.getLine();
        log.info(message + " on line " + line);
    }

    // *** PROGRAM ***

    /**
     * Program = ʺprogramʺ ident {Namespace} {ConstDecl | VarDecl | ClassDecl } ʺ{ʺ {MethodDecl} ʺ}ʺ
     */
    @Override
    public void visit(ProgramName programName) {
        final String name = programName.getProgramName();
        final Obj programObj = Tab.insert(Obj.Prog, name, Tab.noType);
        Tab.openScope();
        state.openScope(Scope.GLOBAL);
        programName.obj = programObj;

        Tab.insert(Obj.Var, DESIGNATOR_COUNTER, Tab.intType);
    }

    @Override
    public void visit(Program program) {
        numberOfVariables = Tab.currentScope().getnVars();
        Tab.chainLocalSymbols(program.getProgramName().obj);
        Tab.closeScope();
        state.closeScope();
    }

    // *** NAMESPACE ***

    @Override
    public void visit(NamespaceName namespaceName) {
        final String namespace = namespaceName.getNamespaceName();
        state.setCurrentNamespace(namespace);
        NameMangler.openNamespaceScope(namespace); // TODO: tag: name_mangler
    }

    @Override
    public void visit(Namespace namespace) {
        state.setCurrentNamespace(null);
        NameMangler.closeNamespaceScope(); // TODO: tag: name_mangler
    }

    // *** CONSTANT DECLARATION ***

    private void visitConstDecl(String name, Struct constType, int value, ConstDeclIdent constDecl) {
        final String constDeclName = NameMangler.mangle(name); // TODO: tag: name_mangler

        final Struct declaredType = state.getCurrentType();

        if (declaredType == Tab.noType) {
            logError("ConstDeclIdent: Declared type is noType", constDecl);
            return;
        }

        final Set<Struct> allowedTypes = Set.of(Tab.intType, Tab.charType, Tab.find(BOOL_TYPE).getType());
        if (!allowedTypes.contains(declaredType)) {
            logError(
                    "ConstDeclIdent: Declared type " + StructType.getTypeName(declaredType.getKind()) +
                            " is not allowed for constants", constDecl
            );
            return;
        }

        final Obj obj = Tab.find(constDeclName);
        if (obj != Tab.noObj) {
            logError("ConstDeclIdent: Constant " + constDeclName + " already exists in symbol table", constDecl);
            return;
        }

        if (declaredType != constType) {
            logError(
                    "SingleConstDeclIdent: Declared type " + StructType.getTypeName(declaredType.getKind()) +
                            " does not match constant type " + StructType.getTypeName(constType.getKind()),
                    constDecl
            );
            return;
        }

        final Obj constObj = Tab.insert(Obj.Con, constDeclName, constType);
        constObj.setAdr(value);

        logInfo("ConstDeclIdent: Constant " + constDeclName + " declared", constDecl);
    }

    /**
     * ident ʺ=ʺ numConst
     */
    @Override
    public void visit(ConstDeclIdentNumber constDecl) {
        final String name = constDecl.getConstName();
        final Struct intType = Tab.intType;
        final int value = constDecl.getValue();
        visitConstDecl(name, intType, value, constDecl);
    }

    /**
     * ident ʺ=ʺ charConst
     */
    @Override
    public void visit(ConstDeclIdentChar constDecl) {
        final String name = constDecl.getConstName();
        final Struct charType = Tab.charType;
        final int value = constDecl.getValue();
        visitConstDecl(name, charType, value, constDecl);
    }

    /**
     * ident ʺ=ʺ boolConst
     */
    @Override
    public void visit(ConstDeclIdentBoolean constDecl) {
        final String name = constDecl.getConstName();
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        final int value = constDecl.getValue() ? 1 : 0;
        visitConstDecl(name, boolType, value, constDecl);
    }

    // *** VARIABLE DECLARATION ***

    private void visitVarDecl(String name, Struct type, VarDeclIdentTerm varDecl) {
        final String varDeclName = NameMangler.mangle(name); // TODO: tag: name_mangler
        final Scope currentScope = state.getCurrentScope();
        int kind = switch (currentScope) {
            case GLOBAL, METHOD_DECL, STATIC_CLASS_VAR_DECL -> Obj.Var;
            case CLASS_DECL -> Obj.Fld;
            default -> Obj.NO_VALUE;
        };

        final Obj obj = Tab.find(varDeclName);
        if (obj != Tab.noObj) {
            logError("VarDeclIdent: Varibale " + varDeclName + " already exists in symbol table", varDecl);
            return;
        }

        final Obj varObj = Tab.insert(kind, varDeclName, type);
        final int level = state.getLevel();
        varObj.setLevel(level);

        if (currentScope == Scope.STATIC_CLASS_VAR_DECL) {
            varObj.setLevel(0);
        }

        logInfo("VarDeclIdent: Varibale " + varDeclName + " declared", varDecl);
    }

    @Override
    public void visit(VarDeclIdent varDecl) {
        final String name = varDecl.getVarName();
        final Struct declaredType = state.getCurrentType();
        if (declaredType == Tab.noType) {
            logError("VarDeclIdent: Declared type is noType", varDecl);
            return;
        }
        visitVarDecl(name, declaredType, varDecl);
    }

    @Override
    public void visit(VarDeclIdentArray varDecl) {
        final String name = varDecl.getVarName();
        final Struct declaredType = state.getCurrentType();
        if (declaredType == Tab.noType) {
            logError("VarDeclIdentArray: Declared type is noType", varDecl);
            return;
        }
        final Struct type = new Struct(Struct.Array, declaredType);
        visitVarDecl(name, type, varDecl);
    }

    // *** CLASS DECLARATION ***

    @Override
    public void visit(ClassName classNameTerm) {
        final String className = classNameTerm.getClassName();
        final String mangledClassName = NameMangler.mangle(className); // TODO: tag: name_mangler

        final Obj node = Tab.find(mangledClassName);
        if (node != Tab.noObj) {
            logError("ClassName: Class " + mangledClassName + " already exists in the symbol table", classNameTerm);
            classNameTerm.obj = Tab.noObj;
            return;
        }

        final Struct classType = new Struct(Struct.Class);
        final Obj classNode = Tab.insert(Obj.Type, mangledClassName, classType);
        classNameTerm.obj = classNode;

        state.setCurrentClass(classNode);
        state.openScope(Scope.CLASS_DECL);

        NameMangler.openNamespaceScope(className); // TODO: tag: name_mangler

        logInfo("ClassName: Class " + mangledClassName + " declared", classNameTerm);
    }

    @Override
    public void visit(ClassDecl classDecl) {
        final Obj classObj = classDecl.getClassDeclStart().obj;
        state.closeScope();
        state.setCurrentClass(null);
        Tab.chainLocalSymbols(classObj.getType());
        Tab.closeScope();
        NameMangler.closeNamespaceScope(); // TODO: tag: name_mangler
    }

    @Override
    public void visit(ClassExtends classExtends) {
        final Struct baseClassType = classExtends.getType().struct;

        if (baseClassType.getKind() != Struct.Class) {
            logError("ClassExtends: Base class is not a class", classExtends);
            classExtends.struct = Tab.noType;
            return;
        }

        classExtends.struct = baseClassType;
    }

    @Override
    public void visit(NoClassExtends classNoExtends) {
        classNoExtends.struct = Tab.noType;
    }

    @Override
    public void visit(StaticVarDeclStart staticVarDecl) {
        state.openScope(Scope.STATIC_CLASS_VAR_DECL);
    }

    private void visitClassDeclStart(ClassDeclStart classDecl) {
        Tab.openScope();
        Tab.insert(Obj.Fld, V_TABLE_PTR, Tab.intType);

        final Obj classObj = classDecl.obj;
        final String className = classObj.getName();
        final Struct classType = classObj.getType();
        final Struct baseClassType = classType.getElemType();

        // If class is derived, set base class type and copy its fields
        if (baseClassType != Tab.noType) {
            // Copy base class fields
            baseClassType
                    .getMembers()
                    .stream()
                    .filter(obj -> !obj.getName().equals(V_TABLE_PTR) && obj.getKind() != Obj.Meth)
                    .forEach(
                            baseClassField -> {
                                final String fieldName = NameMangler.demangle(baseClassField.getName()); // TODO: tag name_mangler
                                final Obj classField = Tab.insert(
                                        Obj.Fld,
                                        NameMangler.mangle(fieldName),
                                        baseClassField.getType()
                                );
                                classField.setAdr(baseClassField.getAdr()); // TODO: check if this is correct
                            }
                    );
        }
    }

    @Override
    public void visit(ClassDeclStartWithoutStatic classDecl) {
        final Obj classObj = classDecl.getClassName().obj;
        final Struct baseClassType = classDecl.getOptionalExtends().struct;
        classObj.getType().setElementType(baseClassType);
        classDecl.obj = classObj;
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticVarDecl classDecl) {
        state.closeScope();
        final Obj classObj = classDecl.getClassName().obj;
        final Struct baseClassType = classDecl.getOptionalExtends().struct;
        classObj.getType().setElementType(baseClassType);
        classDecl.obj = classObj;
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticInitializer classDecl) {
        final Obj classObj = classDecl.getClassName().obj;
        final Struct baseClassType = classDecl.getOptionalExtends().struct;
        classObj.getType().setElementType(baseClassType);
        classDecl.obj = classObj;
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassDeclStartWithStaticVarDeclAndStaticInitializer classDecl) {
        state.closeScope();
        final Obj classObj = classDecl.getClassName().obj;
        final Struct baseClassType = classDecl.getOptionalExtends().struct;
        classObj.getType().setElementType(baseClassType);
        classDecl.obj = classObj;
        visitClassDeclStart(classDecl);
    }

    @Override
    public void visit(ClassVarDeclList classVarDeclList) {
        // After we visited all class variable declarations, we can copy methods from base class
        final Obj currentClass = state.getCurrentClass();
        final Struct baseClassType = currentClass.getType().getElemType();

        if (baseClassType != Tab.noType) {
            baseClassType.getMembers().stream().filter(obj -> obj.getKind() == Obj.Meth).forEach(
                    baseClassMethod -> {
                        final String baseClassMethodName = NameMangler.demangle(baseClassMethod.getName()); // TODO: tag: name_mangler
                        final Obj classMethod = Tab.insert(
                                Obj.Meth,
                                NameMangler.mangle(baseClassMethodName),
                                baseClassMethod.getType()
                        );
                        classMethod.setLevel(baseClassMethod.getLevel());
                        classMethod.setAdr(baseClassMethod.getAdr()); // TODO: check if this is correct
                        Tab.openScope();
                        baseClassMethod.getLocalSymbols()
                                .forEach(symbol -> {
                                    final String baseClassMethodLocalSymbolName = NameMangler.demangle(symbol.getName()); // TODO: tag: name_mangler
                                    final boolean isThisVar = symbol.getName().equals(THIS_VAR);
                                    final Obj classMethodLocalSymbol = Tab.insert(
                                            Obj.Var,
                                            isThisVar ? THIS_VAR : NameMangler.mangle(baseClassMethodLocalSymbolName),
                                            symbol.getType()
                                    );
                                    classMethodLocalSymbol.setLevel(symbol.getLevel());
                                    classMethodLocalSymbol.setAdr(symbol.getAdr()); // TODO: check if this is correct
                                });
                        Tab.chainLocalSymbols(classMethod);
                        Tab.closeScope();
                    }
            );
        }
    }

    @Override
    public void visit(StaticInitializerStart staticInitializerStart) {
        state.setInsideStaticInitializer(true);
    }

    @Override
    public void visit(StaticInitializer staticInitializer) {
        state.setInsideStaticInitializer(false);
    }

    // *** METHOD DECLARATION ***

    private void visitMethodDeclName(String methodName, Struct returnType, MethodTypeName methodTypeName) {
        final String mangledMethodName = NameMangler.mangle(methodName); // TODO: tag: name_mangler
        final Scope currentScope = state.getCurrentScope();
        final Obj currentClass = state.getCurrentClass();

        final boolean insideDerivedClass = currentClass != null && currentClass.getType().getElemType() != Tab.noType;

        final Obj obj = Tab.find(mangledMethodName);
        if (obj != Tab.noObj && !insideDerivedClass) {
            logError("MethodTypeName: Method " + mangledMethodName + " already exists in symbol table", methodTypeName);
            return;
        }

        if (obj != Tab.noObj) {
            Tab.currentScope().getLocals().deleteKey(mangledMethodName);
            logInfo("MethodTypeName: Method " + mangledMethodName + " already exists in symbol table, but it is overriden", methodTypeName);
        }

        final Obj methodObj = Tab.insert(Obj.Meth, mangledMethodName, returnType);
        methodObj.setLevel(0);

        Tab.openScope();

        NameMangler.openNamespaceScope(methodName); // TODO: tag: name_mangler

        if (currentScope == Scope.CLASS_DECL && currentClass != null) {
            final Obj thisObj = Tab.insert(Obj.Var, THIS_VAR, currentClass.getType());
            thisObj.setLevel(2);
            methodObj.setLevel(1);
        }

        state.setCurrentMethod(methodObj);
        state.openScope(Scope.METHOD_DECL);

        methodTypeName.obj = methodObj;

        logInfo("Method " + methodObj.getName() + " declared", methodTypeName);
    }

    @Override
    public void visit(ReturnTypeMethodName returnTypeMethodName) {
        final String methodName = returnTypeMethodName.getMethodName();
        final Struct returnType = returnTypeMethodName.getType().struct;
        visitMethodDeclName(methodName, returnType, returnTypeMethodName);
    }

    @Override
    public void visit(VoidMethodName voidMethodName) {
        final String methodName = voidMethodName.getMethodName();
        final Struct returnType = Tab.noType;
        visitMethodDeclName(methodName, returnType, voidMethodName);
    }

    @Override
    public void visit(MethodDecl methodDecl) {
        final Obj methodObj = methodDecl.getMethodTypeName().obj;
        Tab.chainLocalSymbols(methodObj);

        Tab.closeScope();
        state.setCurrentMethod(null);
        state.closeScope();
        NameMangler.closeNamespaceScope(); // TODO: tag: name_mangler
    }

    // *** FORMAL PARAMETERS ***

    private void visitMethodFormalParameters(String name, Struct type, FormParsTypeIdentTerm formParsTypeIdentTerm) {
        final Obj methodObj = state.getCurrentMethod();
        final String methodName = methodObj.getName();
        final String formParName = methodName + "::" + name; // TODO: tag: name_mangling

        final Obj obj = Tab.find(formParName);
        if (obj != Tab.noObj) {
            logError(
                    "FormParsTypeIdentTerm: Variable " + formParName + " already exists in the symbol table",
                    formParsTypeIdentTerm
            );
            return;
        }

        final Obj varNode = Tab.insert(Obj.Var, formParName, type);
        final int level = state.getLevel();
        varNode.setLevel(level);

        methodObj.setLevel(methodObj.getLevel() + 1);

        logInfo("Formal parameter " + formParName + " declared", formParsTypeIdentTerm);
    }

    @Override
    public void visit(FormParsTypeIdent formParsTypeIdent) {
        final String formParName = formParsTypeIdent.getName();
        final Struct formParType = formParsTypeIdent.getType().struct;
        visitMethodFormalParameters(formParName, formParType, formParsTypeIdent);
    }

    @Override
    public void visit(FormParsTypeIdentArray formParsTypeIdentArray) {
        final String formParName = formParsTypeIdentArray.getName();
        final Struct formParType = formParsTypeIdentArray.getType().struct;
        visitMethodFormalParameters(formParName, new Struct(Struct.Array, formParType), formParsTypeIdentArray);
    }

    // *** TYPE ***

    private void visitType(String typeName, Type type) {
        final Obj typeObj = NameMangler.findName(typeName); // TODO: tag: name_mangler
        if (typeObj == Tab.noObj) {
            logError("Type: Type " + typeName + " not found in symbol table! ", type);
            type.struct = Tab.noType;
            state.setCurrentType(Tab.noType);
            return;
        }
        if (typeObj.getKind() != Obj.Type) {
            logError("Type: " + typeName + " is not a type! ", type);
            type.struct = Tab.noType;
            state.setCurrentType(Tab.noType);
            return;
        }
        state.setCurrentType(typeObj.getType());
        type.struct = typeObj.getType();
    }

    /**
     * Type = ident ʺ::ʺ ident
     */
    @Override
    public void visit(TypeWithNamespace typeWithNamespace) {
        final String typeName = typeWithNamespace.getTypeName();
        final String namespace = typeWithNamespace.getNamespaceName();
        final String typeWithNamespaceName = namespace + "::" + typeName; // TODO: tag: name_mangling
        visitType(typeWithNamespaceName, typeWithNamespace);
    }

    /**
     * Type = ident
     */
    @Override
    public void visit(TypeWithoutNamespace typeWithoutNamespace) {
        final String typeName = typeWithoutNamespace.getTypeName();
        visitType(typeName, typeWithoutNamespace);
    }

    // *** DESIGNATOR STATEMENT ***

    /**
     * DesignatorStatement = Designator Assignop Expr ʺ;ʺ
     */
    @Override
    public void visit(DesignatorStatementAssignopExpr designatorStatementAssignopExpr) {
        final Obj designatorObj = designatorStatementAssignopExpr.getDesignator().obj;
        final int kind = designatorObj.getKind();

        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            logError(
                    "DesignatorStatementAssignopExpr: Designator in assignment statement is not a variable, " +
                            "array element or class field", designatorStatementAssignopExpr
            );
            return;
        }

        final Struct designatorType = designatorObj.getType();
        final Struct exprType = designatorStatementAssignopExpr.getExpr().struct;

        final boolean designatorIsBaseClassOfExpr = exprType.getElemType() == designatorType;

        if (!exprType.assignableTo(designatorType) && !designatorIsBaseClassOfExpr) {
            logError(
                    "DesignatorStatementAssignopExpr: Expression is not assignable to designator",
                    designatorStatementAssignopExpr
            );
        }
    }

    /**
     * DesignatorStatement = = Designator ʺ++ʺ ʺ;ʺ
     */
    @Override
    public void visit(DesignatorStatementIncrement designatorStatementIncrement) {
        final Obj designatorObj = designatorStatementIncrement.getDesignator().obj;
        final int kind = designatorObj.getKind();

        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            logError(
                    "DesignatorStatementIncrement: Designator in increment statement is not a variable, " +
                            "array element or class field", designatorStatementIncrement
            );
            return;
        }

        if (designatorObj.getType() != Tab.intType) {
            logError(
                    "DesignatorStatementIncrement: Designator in increment statement is not of type int",
                    designatorStatementIncrement
            );
        }
    }

    /**
     * DesignatorStatement = = Designator "--" ʺ;ʺ
     */
    @Override
    public void visit(DesignatorStatementDecrement designatorStatementDecrement) {
        final Obj designatorObj = designatorStatementDecrement.getDesignator().obj;
        final int kind = designatorObj.getKind();

        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            logError(
                    "DesignatorStatementDecrement: Designator in decrement statement is not a variable, " +
                            "array element or class field", designatorStatementDecrement
            );
            return;
        }

        if (designatorObj.getType() != Tab.intType) {
            logError(
                    "DesignatorStatementDecrement: Designator in decrement statement is not of type int",
                    designatorStatementDecrement
            );
        }
    }

    /**
     * DesignatorStatement = "[" {[Designator] ","} ʺ*ʺDesignator "]" "=" Designator
     */
    @Override
    public void visit(DesignatorStatementUnpack designatorStatement) {
        final Obj designatorRightArrayObj = designatorStatement.getDesignator1().obj;
        final Struct designatorRightType = designatorRightArrayObj.getType();
        if (designatorRightType.getKind() != Struct.Array) {
            logError(
                    "DesignatorStatementUnpack: Designator on the right side of the assignment is not an array",
                    designatorStatement
            );
            return;
        }
        final Obj designatorAfterStar = designatorStatement.getDesignator().obj;
        final Struct designatorAfterStarType = designatorAfterStar.getType();
        if (designatorAfterStarType.getKind() != Struct.Array) {
            logError(
                    "DesignatorStatementUnpack: Designator after * is not an array",
                    designatorStatement
            );
            return;
        }

        final Struct arrayElemType = designatorRightType.getElemType();
        final List<Struct> unpackDesignatorTypes = state.getCurrentUnpackDesignatorTypes();
        for (final Struct unpackDesignatorType : unpackDesignatorTypes) {
            if (unpackDesignatorType != Tab.noType && !arrayElemType.compatibleWith(unpackDesignatorType)) {
                logError(
                        "DesignatorStatementUnpack: Designator on the left side of the assignment is not of type " +
                                StructType.getTypeName(arrayElemType.getKind()),
                        designatorStatement
                );
                return;
            }
        }

        if (!designatorAfterStarType.getElemType().compatibleWith(arrayElemType)) {
            logError(
                    "DesignatorStatementUnpack: Designator on the right side of the assignment is not of type " +
                            StructType.getTypeName(designatorAfterStarType.getKind()),
                    designatorStatement
            );
        }

        state.clearCurrentUnpackDesignatorTypes();
    }

    @Override
    public void visit(DesignatorCommaList designatorList) {
        final Obj designatorObj = designatorList.getOptionalDesignator().obj;
        final int kind = designatorObj.getKind();
        if (designatorObj != Tab.noObj && kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            logError(
                    "DesignatorCommaList: Designator in unpack statement is not a variable, " +
                            "array element or class field", designatorList
            );
            return;
        }
        state.addUnpackDesignatorType(designatorObj.getType());
    }

    @Override
    public void visit(DesignatorPresent designator) {
        designator.obj = designator.getDesignator().obj;
    }

    @Override
    public void visit(NoDesignator designator) {
        designator.obj = Tab.noObj;
    }

    // *** STATEMENT ***

    /**
     * Statement = ʺbreakʺ
     */
    @Override
    public void visit(StatementBreak statement) {
        final Scope currentScope = state.getCurrentScope();
        if (currentScope != Scope.FOR_LOOP) {
            logError("StatementBreak: Break statement is not inside a for loop", statement);
        }
    }

    /**
     * Statement = ʺcontinueʺ
     */
    @Override
    public void visit(StatementContinue statement) {
        final Scope currentScope = state.getCurrentScope();
        if (currentScope != Scope.FOR_LOOP) {
            logError("StatementContinue: Continue statement is not inside a for loop", statement);
        }
    }

    /**
     * Statement = ʺreadʺ ʺ(ʺ Designator ʺ)ʺ ʺ;ʺ
     */
    @Override
    public void visit(StatementRead statement) {
        final Obj designatorObj = statement.getDesignator().obj;
        final int kind = designatorObj.getKind();

        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            logError(
                    "StatementRead: Designator in read statement is not a variable, " +
                            "array element or class field", statement
            );
            return;
        }

        final Struct designatorType = designatorObj.getType();
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (designatorType != Tab.intType && designatorType != Tab.charType && designatorType != boolType) {
            logError(
                    "StatementRead: Designator in read statement is not of type int, char or bool",
                    statement
            );
        }
    }

    /**
     * Statement = ʺprintʺ ʺ(ʺ Expr ʺ)ʺ ʺ;ʺ
     */
    @Override
    public void visit(StatementPrint statement) {
        final Struct exprType = statement.getExpr().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (exprType != Tab.intType && exprType != Tab.charType && exprType != boolType) {
            logError(
                    "StatementPrint: Expression in print statement is not of type int, char or bool",
                    statement
            );
        }
    }

    /**
     * Statement = ʺprintʺ ʺ(ʺ Expr ʺ,ʺ numConst ʺ)ʺ ʺ;ʺ
     */
    @Override
    public void visit(StatementPrintWithWidth statement) {
        final Struct exprType = statement.getExpr().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (exprType != Tab.intType && exprType != Tab.charType && exprType != boolType) {
            logError(
                    "StatementPrintWithNumConst: Expression in print statement is not of type int, char or bool",
                    statement
            );
        }
    }

    /**
     * Statement = ʺreturnʺ Expr
     */
    @Override
    public void visit(StatementReturnExpr statement) {
        // we cannot use state.getCurrentScope() because return statement can be in a for loop
        final Obj currentMethodObj = state.getCurrentMethod();
        if (currentMethodObj == null) {
            logError("StatementReturnExpr: Return statement is not inside a method", statement);
            return;
        }
        final Struct exprType = statement.getExpr().struct;
        final Struct currentMethodType = currentMethodObj.getType();
        if (exprType != currentMethodType) {
            logError(
                    "StatementReturnExpr: Expression in return statement is not of type " +
                            StructType.getTypeName(currentMethodType.getKind()),
                    statement
            );
        }
    }

    /**
     * Statement = ʺreturnʺ
     */
    @Override
    public void visit(StatementReturn statement) {
        // we cannot use state.getCurrentScope() because return statement can be in a for loop
        final Obj currentMethodObj = state.getCurrentMethod();
        if (currentMethodObj == null) {
            logError("StatementReturnExpr: Return statement is not inside a method", statement);
            return;
        }
        final Struct currentMethodType = currentMethodObj.getType();
        if (currentMethodType != Tab.noType) {
            logError(
                    "StatementReturn: Return statement must have an expression for methods that return a value",
                    statement
            );
        }
    }

    /**
     * Statement = ʺifʺ ʺ(ʺ Condition ʺ)ʺ Statement
     */
    @Override
    public void visit(StatementIf statement) {
        final Struct conditionType = statement.getCondition().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (conditionType != boolType) {
            logError("StatementIf: Condition in if statement is not of type bool", statement);
        }
    }

    /**
     * Statement = ʺifʺ ʺ(ʺ Condition ʺ)ʺ Statement ʺelseʺ Statement
     */
    @Override
    public void visit(StatementIfElse statement) {
        final Struct conditionType = statement.getCondition().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (conditionType != boolType) {
            logError("StatementIf: Condition in if statement is not of type bool", statement);
        }
    }

    @Override
    public void visit(ForLoopCondition forLoopCondition) {
        final Struct conditionType = forLoopCondition.getCondFact().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();
        if (conditionType != boolType) {
            logError("ForLoopCondition: Condition in for loop is not of type bool", forLoopCondition);
        }
    }

    @Override
    public void visit(ForLoopBodyStart forLoopBodyStart) {
        state.openScope(Scope.FOR_LOOP);
    }

    @Override
    public void visit(ForLoopBody forLoopBody) {
        state.closeScope();
    }

    // *** ACTUAL PARAMETERS ***

    /**
     * ActPars = Expr
     */
    @Override
    public void visit(SingleActPars singleActPars) {
        final Struct exprType = singleActPars.getExpr().struct;
        state.addMethodArgumentType(exprType);
    }

    /**
     * ActPars = ActPars {ʺ,ʺ Expr}
     */
    @Override
    public void visit(ActPars actPars) {
        final Struct exprType = actPars.getExpr().struct;
        state.addMethodArgumentType(exprType);
    }

    // *** METHOD CALLS ***

    @Override
    public void visit(MethodCallStart methodCallStart) {
        final Obj methodObj = methodCallStart.getDesignator().obj;
        final String methodName = methodObj.getName();
        if (methodObj.getKind() != Obj.Meth) {
            logError("MethodCallStart: Method " + methodName + " is not a method", methodCallStart);
            methodCallStart.obj = Tab.noObj;
            return;
        }
        methodCallStart.obj = methodObj;
        state.clearCurrentMethodArgumentTypes();
    }

    @Override
    public void visit(MethodCallNoActPars methodCallNoActPars) {
        final Obj methodObj = methodCallNoActPars.getMethodCallStart().obj;
        final String methodName = methodObj.getName();
        if (methodObj.getKind() != Obj.Meth) {
            logError("MethodCallNoActPars: Method " + methodName + " is not a method", methodCallNoActPars);
            methodCallNoActPars.obj = Tab.noObj;
            return;
        }

        int numberOfParameters = methodObj.getLevel();
        final Optional<Obj> thisPtr = methodObj.getLocalSymbols()
                .stream()
                .filter(obj -> obj.getName().equals(THIS_VAR))
                .findFirst();
        if (thisPtr.isPresent()) {
            numberOfParameters--;
        }

        if (numberOfParameters != 0) {
            logError(
                    "MethodCallNoActPars: Method " + methodName + " has " + numberOfParameters +
                            " parameters, but no arguments were given",
                    methodCallNoActPars
            );
            methodCallNoActPars.obj = Tab.noObj;
            return;
        }

        methodCallNoActPars.obj = methodObj;
    }

    @Override
    public void visit(MethodCallWithActPars methodCallActPars) {
        final Obj methodObj = methodCallActPars.getMethodCallStart().obj;
        final String methodName = methodObj.getName();
        if (methodObj.getKind() != Obj.Meth) {
            logError("MethodCallWithActPars: Method " + methodName + " is not a method", methodCallActPars);
            methodCallActPars.obj = Tab.noObj;
            return;
        }

        final List<Struct> methodActualArgumentTypesList = state.getCurrentMethodArgumentTypes();

        final int numberOfActualArguments = methodActualArgumentTypesList.size();
        final int methodObjLevel = methodObj.getLevel();

        final List<Struct> methodFormalArgumentTypesList = methodObj
                .getLocalSymbols()
                .stream()
                .limit(methodObjLevel)
                .filter(obj -> !obj.getName().equals(THIS_VAR))
                .map(Obj::getType)
                .toList();

        final int numberOfFormalArguments = methodFormalArgumentTypesList.size();

        if (numberOfActualArguments != numberOfFormalArguments) {
            logError(
                    "MethodCallWithActPars: Method " + methodName + " has " + numberOfFormalArguments +
                            " parameters, but " + numberOfActualArguments + " arguments were given", methodCallActPars
            );
            methodCallActPars.obj = Tab.noObj;
            return;
        }

        for (int i = 0; i < numberOfActualArguments; i++) {
            final Struct actualArgumentType = methodActualArgumentTypesList.get(i);
            final Struct formalArgumentType = methodFormalArgumentTypesList.get(i);
            if (!actualArgumentType.assignableTo(formalArgumentType)) {
                logError(
                        "MethodCallWithActPars: Argument " + i + " is not of type " +
                                StructType.getTypeName(formalArgumentType.getKind()), methodCallActPars
                );
                methodCallActPars.obj = Tab.noObj;
            }
        }

        methodCallActPars.obj = methodObj;
        state.clearCurrentMethodArgumentTypes();
    }

    // *** CONDITION ***

    /**
     * Condition = CondTerm
     */
    @Override
    public void visit(SingleCondition condition) {
        final Struct condTermType = condition.getCondTerm().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();

        if (condTermType != boolType) {
            logError("SingleCondition: Condition term is not of type bool", condition);
            condition.struct = Tab.noType;
            return;
        }

        condition.struct = boolType;
    }

    /**
     * Condition = Condition {ʺ||ʺ CondTerm}.
     */
    @Override
    public void visit(ConditionOrCondTerm condition) {
        final Struct conditionType = condition.getCondition().struct;
        final Struct condTermType = condition.getCondTerm().struct;

        final Struct boolType = Tab.find(BOOL_TYPE).getType();

        if (conditionType != boolType) {
            logError("ConditionOrCondTerm: Condition is not of type bool", condition);
            condition.struct = Tab.noType;
            return;
        }

        if (condTermType != boolType) {
            logError("ConditionOrCondTerm: Condition term is not of type bool", condition);
            condition.struct = Tab.noType;
            return;
        }

        condition.struct = boolType;
    }

    // *** CONDITION TERM ***

    /**
     * CondTerm = CondFact
     */
    @Override
    public void visit(SingleCondTerm condTerm) {
        final Struct condFactType = condTerm.getCondFact().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();

        if (condFactType != boolType) {
            logError("SingleCondTerm: Condition fact is not of type bool", condTerm);
            condTerm.struct = Tab.noType;
            return;
        }

        condTerm.struct = boolType;
    }

    /**
     * CondTerm = CondFact {ʺ&&ʺ CondFact}
     */
    @Override
    public void visit(CondTermAndCondFact condTerm) {
        final Struct conTermType = condTerm.getCondTerm().struct;
        final Struct condFactType = condTerm.getCondFact().struct;

        final Struct boolType = Tab.find(BOOL_TYPE).getType();

        if (conTermType != boolType) {
            logError("CondTermAndCondFact: Condition term is not of type bool", condTerm);
            condTerm.struct = Tab.noType;
            return;
        }

        if (condFactType != boolType) {
            logError("CondTermAndCondFact: Condition fact is not of type bool", condTerm);
            condTerm.struct = Tab.noType;
            return;
        }

        condTerm.struct = boolType;
    }

    // *** CONDITION FACT ***

    /**
     * CondFact = Expr
     */
    @Override
    public void visit(SingleCondFact condFact) {
        final Struct exprType = condFact.getExpr().struct;
        final Struct boolType = Tab.find(BOOL_TYPE).getType();

        if (exprType != boolType) {
            logError("SingleCondFact: Expression is not of type bool", condFact);
            condFact.struct = Tab.noType;
            return;
        }

        condFact.struct = boolType;
    }

    /**
     * CondFact = Expr Relop Expr
     */
    @Override
    public void visit(CondFactExprRelopExpr condFact) {
        final Struct firstExprType = condFact.getExpr().struct;
        final Struct secondExprType = condFact.getExpr1().struct;
        final OperatorType operatorType = condFact.getRelop().operatortype;

        if (!firstExprType.compatibleWith(secondExprType)) {
            logError("CondFactExprRelopExpr: Expressions are not of the same type", condFact);
            condFact.struct = Tab.noType;
            return;
        }

        final boolean isClassOrArray = firstExprType.isRefType() && secondExprType.isRefType();

        if (isClassOrArray && operatorType != OperatorType.NOT_EQUAL && operatorType != OperatorType.EQUAL) {
            logError(
                    "CondFactExprRelopExpr: Operator " + operatorType + " is not allowed on class or array types",
                    condFact
            );
            condFact.struct = Tab.noType;
            return;
        }

        condFact.struct = Tab.find(BOOL_TYPE).getType();
    }

    // *** EXPR ***

    /**
     * Expr = Term
     */
    @Override
    public void visit(ExprTerm expr) {
        expr.struct = expr.getTerm().struct;
    }

    /**
     * Expr = "-" Term
     */
    @Override
    public void visit(ExprMinusTerm expr) {
        final Struct termType = expr.getTerm().struct;
        if (termType != Tab.intType) {
            logError("ExprMinusTerm: Term is not of type int", expr);
            expr.struct = Tab.noType;
            return;
        }
        expr.struct = termType;
    }

    /**
     * Expr = Expr Addop Term
     */
    @Override
    public void visit(ExprAddopTerm expr) {
        final Struct exprType = expr.getExpr().struct;
        final Struct termType = expr.getTerm().struct;
        if (exprType != Tab.intType) {
            logError("ExprAddopTerm: Expression is not of type int", expr);
            expr.struct = Tab.noType;
            return;
        }
        if (termType != Tab.intType) {
            logError("ExprAddopTerm: Term is not of type int", expr);
            expr.struct = Tab.noType;
            return;
        }
        expr.struct = exprType;
    }

    // *** TERM ***

    /**
     * Term = Factor
     */
    @Override
    public void visit(SingleFactor term) {
        term.struct = term.getFactor().struct;
    }

    /**
     * Term = Term Mulop Factor
     */
    @Override
    public void visit(TermMulopFactor term) {
        final Struct termType = term.getTerm().struct;
        final Struct factorType = term.getFactor().struct;
        if (termType != Tab.intType) {
            logError("TermMulopFactor: Term is not of type int", term);
            term.struct = Tab.noType;
            return;
        }
        if (factorType != Tab.intType) {
            logError("TermMulopFactor: Factor is not of type int", term);
            term.struct = Tab.noType;
            return;
        }
        term.struct = termType;
    }

    // *** FACTOR ***

    /**
     * Factor = Designator
     */
    @Override
    public void visit(DesignatorFactor factor) {
        factor.struct = factor.getDesignator().obj.getType();
    }

    /**
     * Factor = Designator ʺ(ʺ ʺ)ʺ
     */
    @Override
    public void visit(DesignatorFunctionCallNoActPars factor) {
        factor.struct = factor.getMethodCallNoActPars().obj.getType();
    }

    /**
     * Factor = Designator ʺ(ʺ ActPars ʺ)ʺ
     */
    @Override
    public void visit(DesignatorFunctionCallWithActPars factor) {
        factor.struct = factor.getMethodCallWithActPars().obj.getType();
    }

    /**
     * Factor = numConst
     */
    @Override
    public void visit(NumberConst factor) {
        factor.struct = Tab.intType;
    }

    /**
     * Factor = charConst
     */
    @Override
    public void visit(CharConst factor) {
        factor.struct = Tab.charType;
    }

    /**
     * Factor = boolConst
     */
    @Override
    public void visit(BooleanConst factor) {
        factor.struct = Tab.find(BOOL_TYPE).getType();
    }

    /**
     * Factor = ʺnewʺ Type ʺ(ʺ ʺ)ʺ
     */
    @Override
    public void visit(NewArray factor) {
        final Struct exprType = factor.getExpr().struct;
        if (exprType.getKind() != Struct.Int) {
            logError("FactorNewArray: Type " + StructType.getTypeName(exprType.getKind()) + " is not an int", factor);
            factor.struct = Tab.noType;
            return;
        }
        final Struct type = factor.getType().struct;
        factor.struct = new Struct(Struct.Array, type);
    }

    /**
     * Factor = ʺnewʺ Type ʺ(ʺ [ActPars] ʺ)ʺ
     */
    @Override
    public void visit(NewClassObject factor) {
        final Struct type = factor.getType().struct;
        if (type.getKind() != Struct.Class) {
            logError("FactorNewClassObject: Type " + type.getKind() + " is not a class", factor);
            factor.struct = Tab.noType;
            return;
        }
        factor.struct = type;
    }

    /**
     * Factor = ʺ(ʺ Expr ʺ)ʺ
     */
    @Override
    public void visit(ExprFactor factor) {
        factor.struct = factor.getExpr().struct;
    }


    // *** DESIGNATOR ***

    private void visitDesignatorIdentifier(String name, Designator designator) {
        final Obj designatorObj = NameMangler.findName(name); // TODO: tag: name_mangler
        if (designatorObj == Tab.noObj) {
            logError("DesignatorIdent: Designator " + name + " not found in symbol table", designator);
            designator.obj = Tab.noObj;
            return;
        }

        final Set<Integer> possibleKinds = Set.of(Obj.Var, Obj.Con, Obj.Meth, Obj.Type, Obj.Fld);
        if (!possibleKinds.contains(designatorObj.getKind())) {
            logError(
                    "DesignatorIdent: Designator " + name + " is not a variable, constant, method or type",
                    designator
            );
            designator.obj = Tab.noObj;
            return;
        }

        designator.obj = designatorObj;
    }

    /**
     * Designator = ident ʺ::ʺ ident
     */
    @Override
    public void visit(DesignatorIdentWithNamespace designator) {
        final String namespace = designator.getNamespaceName();
        final String name = designator.getDesignatorName();
        final String designatorName = namespace + "::" + name; // TODO: tag: name_mangling
        visitDesignatorIdentifier(designatorName, designator);
    }

    /**
     * Designator = ident
     */
    @Override
    public void visit(DesignatorIdentWithoutNamespace designator) {
        String name = designator.getDesignatorName();
        visitDesignatorIdentifier(name, designator);
    }

    /**
     * Designator = Designator ʺ.ʺ ident
     */
    @Override
    public void visit(DesignatorClassMember designatorField) {
        Obj designatorObj = designatorField.getDesignator().obj;
        final String designatorName = designatorObj.getName();

        if (designatorObj == Tab.noObj) {
            designatorField.obj = Tab.noObj;
            return;
        }

        // this pointer
        if (designatorName.equals(THIS_VAR) && state.isInsideClass()) {
            designatorObj = state.getCurrentClass();
        }

        final Struct designatorType = designatorObj.getType();

        if (designatorType.getKind() != Struct.Class) {
            logError("DesignatorClassMember: Designator " + designatorName + " is not a class", designatorField);
            designatorField.obj = Tab.noObj;
            return;
        }

        final String classMemberName = designatorField.getClassMemberName();
        final String mangledClassMemberName = "::" + classMemberName;

        // Class member in class which is currently being declared
        final Optional<Obj> classMemberInCurrentClass = Tab.currentScope().getOuter().getLocals().symbols()
                .stream()
                .filter(obj -> obj.getName().endsWith(mangledClassMemberName))
                .findFirst();

        // Class member in class which is already declared
        final Optional<Obj> classMemberInDeclaredClass = designatorType.getMembers()
                .stream()
                .filter(obj -> obj.getName().endsWith(mangledClassMemberName))
                .findFirst();

        // Static class member name
        final Obj staticClassMember = Tab.find(designatorName + mangledClassMemberName);

        if (classMemberInCurrentClass.isEmpty() && classMemberInDeclaredClass.isEmpty() && staticClassMember == Tab.noObj) {
            logError(
                    "DesignatorClassMember: Designator " + designatorName + " does not have member " + mangledClassMemberName,
                    designatorField
            );
            designatorField.obj = Tab.noObj;
            return;
        }

        final Obj classMemberObj = classMemberInCurrentClass
                .orElseGet(
                        () -> classMemberInDeclaredClass.orElse(staticClassMember)
                );

        if (classMemberObj.getKind() != Obj.Fld && classMemberObj.getKind() != Obj.Meth && !(classMemberObj.getKind() == Obj.Var && staticClassMember != Tab.noObj)) {
            logError(
                    "DesignatorClassMember: Designator " + designatorName
                            + " member " + mangledClassMemberName + " is not a field or method",
                    designatorField
            );
            designatorField.obj = Tab.noObj;
            return;
        }

        designatorField.obj = classMemberObj;
    }

    /**
     * Designator = Designator ʺ[ʺ Expr ʺ]ʺ
     */
    @Override
    public void visit(DesignatorArray designator) {
        final Obj designatorObj = designator.getDesignator().obj;
        final Struct designatorType = designatorObj.getType();
        if (designatorType.getKind() != Struct.Array) {
            logError("DesignatorArray: Designator " + designatorObj.getName() + " is not an array", designator);
            designator.obj = Tab.noObj;
            return;
        }

        final Struct exprType = designator.getExpr().struct;
        if (exprType.getKind() != Struct.Int) {
            logError("DesignatorArray: Array index must be of type int", designator);
            designator.obj = Tab.noObj;
            return;
        }

        designator.obj = new Obj(Obj.Elem, "", designatorType.getElemType());
    }

    // *** OPERATORS ***

    @Override
    public void visit(Assign assign) {
        assign.operatortype = OperatorType.ASSIGN;
    }

    @Override
    public void visit(Equal equal) {
        equal.operatortype = OperatorType.EQUAL;
    }

    @Override
    public void visit(NotEqual notEqual) {
        notEqual.operatortype = OperatorType.NOT_EQUAL;
    }

    @Override
    public void visit(GreaterThan greaterThan) {
        greaterThan.operatortype = OperatorType.GREATER_THAN;
    }

    @Override
    public void visit(GreaterOrEqual greaterOrEqual) {
        greaterOrEqual.operatortype = OperatorType.GREATER_OR_EQUAL;
    }

    @Override
    public void visit(LessThan lessThan) {
        lessThan.operatortype = OperatorType.LESS_THAN;
    }

    @Override
    public void visit(LessOrEqual lessOrEqual) {
        lessOrEqual.operatortype = OperatorType.LESS_OR_EQUAL;
    }

    @Override
    public void visit(Plus plus) {
        plus.operatortype = OperatorType.PLUS;
    }

    @Override
    public void visit(Minus minus) {
        minus.operatortype = OperatorType.MINUS;
    }

    @Override
    public void visit(Multiply multiply) {
        multiply.operatortype = OperatorType.MULTIPLY;
    }

    @Override
    public void visit(Divide divide) {
        divide.operatortype = OperatorType.DIVIDE;
    }

    @Override
    public void visit(Mod mod) {
        mod.operatortype = OperatorType.MOD;
    }

}
