package rs.ac.bg.etf.pp1.jump_states;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class IfElseJumpsState {
    private final List<Integer> onElseJumpAddressList = new LinkedList<>();
    private final List<Integer> onThenJumpAddressList = new LinkedList<>();
    private final List<Integer> afterThenJumpAddressList = new LinkedList<>();
    private final List<Integer> onNextOrJumpAddressList = new LinkedList<>();
    private final HashMap<Integer, IfCondition> ifConditionsMap;
    private boolean hasElse = false;

    public IfElseJumpsState(HashMap<Integer, IfCondition> ifConditionsMap) {
        this.ifConditionsMap = ifConditionsMap;
    }

    public void addOnElseJumpAddress(int onElseJumpAddress) {
        onElseJumpAddressList.add(onElseJumpAddress);
    }

    public void addOnThenJumpAddress(int onThenJumpAddress) {
        onThenJumpAddressList.add(onThenJumpAddress);
    }

    public void addAfterThenJumpAddress(int afterThenJumpAddress) {
        afterThenJumpAddressList.add(afterThenJumpAddress);
    }

    public void addOnNextOrJumpAddress(int onNextOrJumpAddress) {
        onNextOrJumpAddressList.add(onNextOrJumpAddress);
    }

    public List<Integer> getOnElseJumpAddressList() {
        return onElseJumpAddressList;
    }

    public List<Integer> getOnThenJumpAddressList() {
        return onThenJumpAddressList;
    }

    public List<Integer> getAfterThenJumpAddressList() {
        return afterThenJumpAddressList;
    }

    public List<Integer> getOnNextOrJumpAddressList() {
        return onNextOrJumpAddressList;
    }

    public boolean isHasElse() {
        return hasElse;
    }

    public void setHasElse(boolean hasElse) {
        this.hasElse = hasElse;
    }

    public HashMap<Integer, IfCondition> getIfConditionsMap() {
        return ifConditionsMap;
    }

    public IfCondition getIfCondition(int condFactHash) {
        return ifConditionsMap.get(condFactHash);
    }
}
