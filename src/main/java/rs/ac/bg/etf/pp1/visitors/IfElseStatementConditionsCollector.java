package rs.ac.bg.etf.pp1.visitors;

import rs.ac.bg.etf.pp1.ast.*;

import java.util.LinkedList;

public class IfElseStatementConditionsCollector extends VisitorAdaptor {

    private final LinkedList<LinkedList<CondFact>> condFactList = new LinkedList<>();

    private int level = 0;


    public void visit(ConditionOrCondTerm conditionOrCondTerm) {
        condFactList.add(new LinkedList<>());
        level++;
    }

    public void visit(SingleCondition singleCondition) {
        condFactList.add(new LinkedList<>());
        level++;
    }

    public void visit(CondTermAndCondFact condTerm) {
        condFactList.get(level - 1).addFirst(condTerm.getCondFact());
    }

    public void visit(SingleCondTerm condTerm) {
        condFactList.get(level - 1).addFirst(condTerm.getCondFact());
        level--;
    }

    public LinkedList<LinkedList<CondFact>> getCondFactList() {
        // since we are traversing the tree bottom up, we need to reverse the list
        final LinkedList<LinkedList<CondFact>> reversedCondFactList = new LinkedList<>();
        for (LinkedList<CondFact> condFact : condFactList) {
            reversedCondFactList.addFirst(condFact);
        }
        return reversedCondFactList;
    }
}
