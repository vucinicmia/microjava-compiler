package rs.ac.bg.etf.pp1.utils;

import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;

import java.util.Optional;
import java.util.Stack;

/**
 * Name mangler is used to mangle names of variables, functions and types.
 * Inspired by C++ name mangling.
 * Link: https://en.wikipedia.org/wiki/Name_mangling
 */
public class NameMangler {

    private static final String DELIMITER = "::";

    private static final Stack<String> namespaceStack = new Stack<>();

    public static Optional<String> getCurrentNamespace() {
        if (namespaceStack.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(namespaceStack.peek());
    }

    public static Optional<String> getOuterNamespace() {
        if (namespaceStack.size() < 2) {
            return Optional.empty();
        }
        return Optional.of(namespaceStack.get(namespaceStack.size() - 2));
    }

    /**
     * Mangles name with current namespace.
     * If there is no namespace, name is returned.
     *
     * @param name
     * @return
     */
    public static String mangle(String name) {
        final StringBuilder mangledName = new StringBuilder();
        getCurrentNamespace().ifPresentOrElse(
                namespace -> mangledName.append(namespace).append(DELIMITER).append(name),
                () -> mangledName.append(name)
        );
        return mangledName.toString();
    }

    /**
     * Opens new namespace scope. Namespace name is appended to current namespace.
     * Should be called before entering new scope.
     * Scopes are: GLOBAL, CLASS, METHOD
     */
    public static void openNamespaceScope(String namespaceName) {
        final StringBuilder mangledName = new StringBuilder();
        getCurrentNamespace().ifPresentOrElse(
                namespace -> mangledName.append(namespace).append(DELIMITER).append(namespaceName),
                () -> mangledName.append(namespaceName)
        );
        namespaceStack.push(mangledName.toString());
    }

    public static void closeNamespaceScope() {
        namespaceStack.pop();
    }

    /**
     * Searches for name in symbol table.
     *
     * @param name
     */
    public static Obj findName(String name) {
        final Stack<String> stack = new Stack<>();
        namespaceStack.forEach(stack::push);
        while (!stack.isEmpty()) {
            final String currentNamespace = stack.pop();
            final String fullName = currentNamespace + DELIMITER + name;
            final Obj obj = Tab.find(fullName);
            if (obj != Tab.noObj) {
                return obj;
            }
        }
        return Tab.find(name);
    }

    public static String demangle(String name) {
        final String[] parts = name.split(DELIMITER);
        return parts[parts.length - 1];
    }

}
