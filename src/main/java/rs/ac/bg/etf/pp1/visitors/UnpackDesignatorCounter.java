package rs.ac.bg.etf.pp1.visitors;

import rs.ac.bg.etf.pp1.ast.DesignatorPresent;
import rs.ac.bg.etf.pp1.ast.DesignatorStatementUnpack;
import rs.ac.bg.etf.pp1.ast.NoDesignator;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.etf.pp1.symboltable.concepts.Obj;

public class UnpackDesignatorCounter extends VisitorAdaptor {

        private int unpackDesignatorsCounter = 0;
        private Obj sourceArrayObj = null;

        public int getUnpackDesignatorsCounter() {
            return unpackDesignatorsCounter;
        }

        public void visit(DesignatorPresent unpackDesignator) {
            unpackDesignatorsCounter++;
        }

        @Override
        public void visit(NoDesignator noDesignator) {
            unpackDesignatorsCounter++;
        }

        @Override
        public void visit(DesignatorStatementUnpack designatorStatementUnpack) {
            sourceArrayObj = designatorStatementUnpack.getDesignator1().obj;
        }

        public Obj getSourceArrayObj() {
            return sourceArrayObj;
        }
}

