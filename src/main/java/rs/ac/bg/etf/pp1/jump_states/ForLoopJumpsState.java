package rs.ac.bg.etf.pp1.jump_states;

import java.util.LinkedList;
import java.util.List;

public class ForLoopJumpsState {
    private final List<Integer> afterLoopJumpAddressList = new LinkedList<>();
    private final List<Integer> onUpdationStatementJumpAddressList = new LinkedList<>();
    private int conditionCheckAddress;

    public void addAfterLoopJumpAddress(int afterLoopJumpAddress) {
        afterLoopJumpAddressList.add(afterLoopJumpAddress);
    }

    public List<Integer> getAfterLoopJumpAddressList() {
        return afterLoopJumpAddressList;
    }

    public void addOnUpdationStatementJumpAddress(int onUpdationStatementJumpAddress) {
        onUpdationStatementJumpAddressList.add(onUpdationStatementJumpAddress);
    }

    public List<Integer> getOnUpdationStatementJumpAddressList() {
        return onUpdationStatementJumpAddressList;
    }

    public int getConditionCheckAddress() {
        return conditionCheckAddress;
    }

    public void setConditionCheckAddress(int conditionCheckAddress) {
        this.conditionCheckAddress = conditionCheckAddress;
    }
}
