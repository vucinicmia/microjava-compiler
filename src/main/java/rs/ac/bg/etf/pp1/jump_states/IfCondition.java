package rs.ac.bg.etf.pp1.jump_states;

import rs.ac.bg.etf.pp1.ast.CondFact;

public class IfCondition {
    private boolean isLastInRow = false;
    private boolean isInLastRow = false;
    private boolean isInOnlyRow = false;
    private CondFact condFact;

    public IfCondition(CondFact condFact) {
        this.condFact = condFact;
    }

    public boolean isLastInRow() {
        return isLastInRow;
    }

    public void setLastInRow(boolean lastInRow) {
        isLastInRow = lastInRow;
    }

    public boolean isInLastRow() {
        return isInLastRow;
    }

    public void setInLastRow(boolean inLastRow) {
        isInLastRow = inLastRow;
    }

    public boolean isInOnlyRow() {
        return isInOnlyRow;
    }

    public void setInOnlyRow(boolean inOnlyRow) {
        this.isInOnlyRow = inOnlyRow;
    }

    public CondFact getCondFact() {
        return condFact;
    }

    public void setCondFact(CondFact condFact) {
        this.condFact = condFact;
    }

}
