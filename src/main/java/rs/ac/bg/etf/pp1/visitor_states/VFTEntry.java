package rs.ac.bg.etf.pp1.visitor_states;

import rs.etf.pp1.mj.runtime.Code;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class VFTEntry {
    private Integer address = null;
    private final List<Integer> toFixAddresses = new LinkedList<>();

    public Optional<Integer> getAddress() {
        return Optional.ofNullable(address);
    }

    public void setAddress(int address) {
        this.address = address;
        toFixAddresses.forEach(toFixAddress -> Code.put2(toFixAddress, address));
        toFixAddresses.clear();
    }

    public List<Integer> getToFixAddresses() {
        return toFixAddresses;
    }

    public void addToFixAddress(int toFixAddress) {
        toFixAddresses.add(toFixAddress);
    }

}
