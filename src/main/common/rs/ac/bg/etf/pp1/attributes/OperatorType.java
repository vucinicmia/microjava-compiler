package rs.ac.bg.etf.pp1.attributes;

public enum OperatorType {
    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE,
    MOD,
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    GREATER_OR_EQUAL,
    LESS_THAN,
    LESS_OR_EQUAL,
    LOGICAL_AND,
    LOGICAL_OR,
    ASSIGN
}
