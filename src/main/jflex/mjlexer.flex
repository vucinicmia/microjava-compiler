
package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{

    /**
    * Creates new symbol with line and column information.
    * @param type type of the symbol
    * @return Symbol - new symbol
    */
    private Symbol newSymbol(int type) {
        return new Symbol(type, yyline + 1, yycolumn);
    }

    /**
    * Creates new symbol with value, line and column information.
    * @param type type of the symbol
    * @param value value of the symbol
    * @return Symbol - new symbol
    */
    private Symbol newSymbol(int type, Object value) {
        return new Symbol(type, yyline + 1, yycolumn, value);
    }

%}

%cup
%line
%column

%xstate COMMENT

%eofval{
	return newSymbol(sym.EOF);
%eofval}

%%

" " 	{ }
"\b" 	{ }
"\t" 	{ }
"\r\n" 	{ }
"\f" 	{ }

/*
    ****************
    *** Keywords ***
    ****************
*/

"program"   { return newSymbol(sym.PROGRAM, yytext()); }
"break"     { return newSymbol(sym.BREAK, yytext()); }
"class"     { return newSymbol(sym.CLASS, yytext()); }
"else"      { return newSymbol(sym.ELSE, yytext()); }
"const"     { return newSymbol(sym.CONST, yytext()); }
"if"        { return newSymbol(sym.IF, yytext()); }
"new"       { return newSymbol(sym.NEW, yytext()); }
"print"     { return newSymbol(sym.PRINT, yytext()); }
"read"      { return newSymbol(sym.READ, yytext()); }
"return"    { return newSymbol(sym.RETURN, yytext()); }
"void"      { return newSymbol(sym.VOID, yytext()); }
"extends"   { return newSymbol(sym.EXTENDS, yytext()); }
"continue"  { return newSymbol(sym.CONTINUE, yytext()); }
"for"       { return newSymbol(sym.FOR, yytext()); }
"static"    { return newSymbol(sym.STATIC, yytext()); }
"namespace" { return newSymbol(sym.NAMESPACE, yytext()); }

/*
    *****************
    *** Operators ***
    *****************
*/

"+"         { return newSymbol(sym.PLUS, yytext()); }
"-"         { return newSymbol(sym.MINUS, yytext()); }
"*"         { return newSymbol(sym.MULTIPLY, yytext()); }
"/"         { return newSymbol(sym.DIVIDE, yytext()); }
"%"         { return newSymbol(sym.MOD, yytext()); }

"=="        { return newSymbol(sym.EQUAL, yytext()); }
"!="        { return newSymbol(sym.NOT_EQUAL, yytext()); }
">"         { return newSymbol(sym.GREATER_THAN, yytext()); }
">="        { return newSymbol(sym.GREATER_OR_EQUAL, yytext()); }
"<"         { return newSymbol(sym.LESS_THAN, yytext()); }
"<="        { return newSymbol(sym.LESS_OR_EQUAL, yytext()); }

"&&"        { return newSymbol(sym.LOGICAL_AND, yytext()); }
"||"        { return newSymbol(sym.LOGICAL_OR, yytext()); }

"="         { return newSymbol(sym.ASSIGN, yytext()); }
"++"        { return newSymbol(sym.INCREMENT, yytext()); }
"--"        { return newSymbol(sym.DECREMENT, yytext()); }

";"         { return newSymbol(sym.SEMICOLON, yytext()); }
":"         { return newSymbol(sym.COLON, yytext()); }
","         { return newSymbol(sym.COMMA, yytext()); }
"."         { return newSymbol(sym.DOT, yytext()); }

"("         { return newSymbol(sym.LPAREN, yytext()); }
")"         { return newSymbol(sym.RPAREN, yytext()); }
"["         { return newSymbol(sym.LBRACKET, yytext()); }
"]"         { return newSymbol(sym.RBRACKET, yytext()); }
"{"         { return newSymbol(sym.LBRACE, yytext()); }
"}"         { return newSymbol(sym.RBRACE, yytext()); }
"=>"        { return newSymbol(sym.ARROW, yytext()); }



/*
    ****************
    *** Comments ***
    ****************
 */

"//"              { yybegin(COMMENT); }
<COMMENT> .       { yybegin(COMMENT); }
<COMMENT> "\r\n"  { yybegin(YYINITIAL); }

/*
    ****************
    *** Literals ***
    ****************
*/

[0-9]+                          { return newSymbol(sym.NUMBER_CONST, Integer.parseInt(yytext())); }
"true"                          { return newSymbol(sym.BOOLEAN_CONST, true); }
"false"                         { return newSymbol(sym.BOOLEAN_CONST, false); }
'.'                             { return newSymbol(sym.CHAR_CONST, yytext().charAt(1)); }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* 	{ return newSymbol(sym.IDENTIFIER, yytext()); }

.                               { System.err.println("Lexer error - [{0}] on line {1}.".format(yytext(), yyline + 1)); }
