package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

public class LexerTest {

    private static final String TEST_INPUT_DIR = "test/microjava/";
    private static final String TEST_OUTPUT_DIR = "test/expected/lexer/";

    private static final Logger log = LoggerFactory.getLogger(LexerTest.class);

    private void executeTest(final String inputFileName, final String expectedOutputFileName) {
        final String inputFilePath = TEST_INPUT_DIR + inputFileName;
        final String outputFilePath = TEST_OUTPUT_DIR + expectedOutputFileName;

        try {
            final Reader inputFileReader = new BufferedReader(new FileReader(inputFilePath));
            final Yylex lexer = new Yylex(inputFileReader);

            final BufferedReader expectedOutputFileReader = new BufferedReader(new FileReader(outputFilePath));

            Symbol currToken = lexer.next_token();
            String expectedValue;

            while (currToken.sym != sym.EOF && (expectedValue = expectedOutputFileReader.readLine()) != null) {

                assert (currToken.value.toString().equals(expectedValue));

                log.info(currToken.sym + " " + currToken.value);
                currToken = lexer.next_token();
            }

            assert (currToken.sym == sym.EOF);

        } catch (final Exception e) {
            log.error(e.getMessage());
        }
    }

    @Test
    public void test301() {
        final String inputFileName = "test301.mj";
        final String expectedOutputFileName = inputFileName.replace(".mj", "-output.txt");

        executeTest(inputFileName, expectedOutputFileName);
    }

    @Test
    public void test302() {
        final String inputFileName = "test302.mj";
        final String expectedOutputFileName = inputFileName.replace(".mj", "-output.txt");

        executeTest(inputFileName, expectedOutputFileName);
    }

    @Test
    public void test303() {
        final String inputFileName = "test303.mj";
        final String expectedOutputFileName = inputFileName.replace(".mj", "-output.txt");

        executeTest(inputFileName, expectedOutputFileName);
    }
}
