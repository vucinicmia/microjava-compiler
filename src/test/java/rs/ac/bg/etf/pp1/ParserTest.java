package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.ac.bg.etf.pp1.ast.Program;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;

import java.io.*;

public class ParserTest {
    private static final String TEST_INPUT_DIR = "test/microjava/";

    private static final Logger log = LoggerFactory.getLogger(LexerTest.class);

    private void executeTest(final String inputFileName) throws Exception {
        final String inputFilePath = TEST_INPUT_DIR + inputFileName;

        Reader reader = null;
        try {
            final File sourceCode = new File(inputFilePath);
            log.info("Compiling source file: " + sourceCode.getAbsolutePath());

            reader = new BufferedReader(new FileReader(sourceCode));
            final Yylex lexer = new Yylex(reader);

            final MJParser p = new MJParser(lexer);
            final Symbol s = p.parse();

            final Program program = (Program)(s.value);
            Tab.init();

            log.info(program.toString(""));
            log.info("===================================");

            final SemanticAnalyzer semanticAnalyzer = new SemanticAnalyzer();
            program.traverseBottomUp(semanticAnalyzer);

            Tab.dump();

            if (!p.errorDetected && !semanticAnalyzer.isErrorDetected()) {
                final CodeGenerator codeGenerator = new CodeGenerator();
                program.traverseBottomUp(codeGenerator);
                Code.dataSize = SemanticAnalyzer.getNumberOfVariables();
                Code.mainPc = codeGenerator.getMainPc();
                final File objectFile = new File("test/out/program.obj");
                if (objectFile.exists()) {
                    boolean successfullyDeleted = objectFile.delete();
                    if (!successfullyDeleted) {
                        log.error("Failed to delete object file: " + objectFile.getAbsolutePath());
                    }
                }
                Code.write(new FileOutputStream(objectFile));
                log.info("===================================");
                log.info("Parsing successfully completed");
            }
            else {
                log.info("===================================");
                log.error("Parsing failed");
            }

            log.info("===================================");

        }
        finally {
            if (reader != null) try { reader.close(); } catch (IOException e1) { log.error(e1.getMessage(), e1); }
        }
    }

    @Test
    public void test301() throws Exception {
        executeTest("test301.mj");
    }

    @Test
    public void test302() throws Exception {
        executeTest("test302.mj");
    }

    @Test
    public void test303() throws Exception {
        executeTest("test303.mj");
    }

    @Test
    public void test303_discord() throws Exception {
        executeTest("test303-discord.mj");
    }

    @Test
    public void test_array_indexing() throws Exception {
        executeTest("test-array-indexing.mj");
    }

    @Test
    public void test_00() throws Exception {
        executeTest("test-00.mj");
    }

    @Test
    public void test300() throws Exception {
        executeTest("test300.mj");
    }

    @Test
    public void test_vezbe_sym_table_01() throws Exception {
        executeTest("test-vezbe-sym-table-01.mj");
    }

    @Test
    public void test_vezbe_sym_table_02() throws Exception {
        executeTest("test-vezbe-sym-table-02.mj");
    }

    @Test
    public void test_vezbe_sym_table_03() throws Exception {
        executeTest("test-vezbe-sym-table-03.mj");
    }

    @Test
    public void test_vezbe_code_gen_01() throws Exception {
        executeTest("test-vezbe-code-gen-01.mj");
    }

    @Test
    public void test_vezbe_code_gen_02() throws Exception {
        executeTest("test-vezbe-code-gen-02.mj");
    }

    @Test
    public void test_vezbe_code_gen_03() throws Exception {
        executeTest("test-vezbe-code-gen-03.mj");
    }

    @Test
    public void test_vezbe_code_gen_04() throws Exception {
        executeTest("test-vezbe-code-gen-04.mj");
    }

}
